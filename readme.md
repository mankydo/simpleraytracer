Kinda semi-complicated raytracer
====

A weekend long project writing c++ raytracer that missed it's deadline by an year.
----

<br> 
This is a simple raytracer done for learning purposes that grown way out of scope. While it's main purpose was to render spheres and two or three materials a feature creep happened since then and atm the raytracer can:

* [x] Render spheres
* [x] Render two materials: Lambert(diffuse) and metalic(reflective with roughness)
* [x] Rendering images using multiple threads os-agnostic(works on linux/windows/macOS)
* [x] Internal memory and object manager(keeps tracks of any object created and provides ABI for managing these objects; anything in the program is an object: objects, scenes, cameras, materials)
* [x] Read and parse a scene description from a .txt file(objects, materials, scenes to render, everything can be setup via a settings file)
* [x] Output images in ppm format
* [x] render images in passes(front, background, depth)
* [x] Custom aspect ratio and field-of-view
* [x] Infinite plane objects
* [ ] xy-bounded plane objects
* [x] Box objects
* [x] A free-positioned camera
* [x] Depth-Of-Field
* [x] Better Depth-Of-Field aperture disks(it is looking very sub-optimal now on higher aperture values)
* [x] Depth-Of-Field blade apperture(any number of blades and a separate disk rotation)
* [ ] Bounding boxes
* [ ] Triagles
* [ ] Refraction material
* [ ] Emissive material
* [ ] importing triangle-based meshes
* [ ] Directional lights
* [ ] maybe point lights as well
* [ ] SDF objects
* [ ] render region(and maybe by tiles in multithread)
* [ ] DOCUMENTATION



<br>

Works on Linux/Windows and has no dependencies other than c++11(linux binary compiled on amd64/ubuntu 18.04 so a recompile for your system may be required).

