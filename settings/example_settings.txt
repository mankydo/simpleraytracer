//an example settings file
// color format: (1,1,1,1)
// position format: (1,1,1)

<settings>
	[width]:
	[height]:
	[samples]:
	[bounces]:
	[scene]:
</settings>

<scene>
	[name]:
	[addobject]:
	[addobject]:
</scene>

<background>
	[name]:
	[colorup]:
	[colorbottom]:
</background>

<material>
	[name]:
	[type]:
	[color]:
</material>

<object>
	[name]:
	[type]:
	[position]:
</object>
