  # the compiler: gcc for C program, define as g++ for C++
CXX = g++

  # compiler flags:
  #  -g    adds debugging information to the executable file
  #  -Wall turns on most, but not all, compiler warnings
SRCDIR = sources
OBJDIR = obj
INCLUDEDIR = includes
BUILDDIR = build

DIRECTORIES = Materials src ParseObjectSettings Objects
SOURCES = $(foreach dir, $(DIRECTORIES), $(wildcard $(SRCDIR)/$(dir)/*.cpp))


INCLUDES = $(foreach dir, $(DIRECTORIES), $(wildcard $(SRCDIR)/$(dir)/$(INCLUDEDIR)/*.h))
INCLUDEDIRS = $(addprefix -I,$(foreach dir, $(DIRECTORIES), $(SRCDIR)/$(dir)/$(INCLUDEDIR)))
OBJECTS = $(addprefix $(OBJDIR)/,$(subst .cpp,.o, $(notdir $(SOURCES))))

FILTER = %Object.o
VPATH = $(addprefix $(SRCDIR)/,$(DIRECTORIES))

CPPFLAGS  = -g  -pthread -std=c++1y -Wall $(INCLUDEDIRS) -I$(SRCDIR)/$(INCLUDEDIR)


  # the build target executable:
TARGET = main

all: $(BUILDDIR)/$(TARGET)

$(BUILDDIR)/$(TARGET): $(OBJECTS)
	@echo "linking "$<" to "$(BUILDDIR)/$(TARGET)"\n"

	$(CXX) $(CPPFLAGS) -o $(BUILDDIR)/$(TARGET) $(addprefix $(OBJDIR)/,$(notdir $(OBJECTS)))
	@echo "Linking "$^" succeed.\n"
	@echo "	Building "$@" is a success!\n"

$(OBJECTS): $(OBJDIR)/%.o : %.cpp ;@echo "\n get arg: "$@" and "$<""
	@echo "Compiling "$<" to "$(OBJDIR)/$(notdir $@)"\n"

	$(CXX) $(CPPFLAGS) -c $< -o $(OBJDIR)/$(notdir $@)

	@echo "Compiled "$<" successfully!\n"

clean:
	$(RM) $(TARGET) $(addprefix $(OBJDIR)/,$(notdir $(OBJECTS)))