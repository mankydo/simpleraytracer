#include "Setting.h"

bool ParseObjectSettings::Setting::ComparePrioritiesUniquePtrs(const std::unique_ptr<ParseObjectSettings::Setting> & a, const std::unique_ptr<ParseObjectSettings::Setting> & b)
{
//    return b.get()->priority > a.get()->priority;
    return ComparePriorities(a.get(), b.get());
}

bool ParseObjectSettings::Setting::ComparePriorities(
    const ParseObjectSettings::Setting * a, 
    const ParseObjectSettings::Setting * b)
{
    return b->priority > a->priority;
}
