#include <fstream>
#include <iostream>
#include <algorithm>
#include <cctype>
#include "Parsers.h"
#include "ParsersHelperFunctions.h"
#include "RtUtilities.h"
#include "RenderSettingsParser.h"

std::string ParseObjectSettings::GetName(const std::string line, const std::string openingChar, const std::string closingChar){
    std::string result = "";

	std::size_t positionStart = line.find(openingChar);
	if( positionStart != std::string::npos) {
		
        std::size_t positionEnd = line.find(closingChar);

		if(positionEnd != std::string::npos){
            std::size_t openingCharSize = openingChar.size();

			result = line.substr(positionStart + openingCharSize, positionEnd - positionStart - openingCharSize);
        } else {
			result = "-1";
        }

    } else {
		result = "-1";
    }

    RtUtilities::StringToLowercase(result);

	return result;
}

std::string ParseObjectSettings::GetProperryValue(const std::string line, const std::string closingChar){
    
    std::string propertyValue = "";
    std::size_t valuePos = line.find(closingChar);

    if(valuePos != std::string::npos){
        propertyValue = line.substr(valuePos + closingChar.size());
        RtUtilities::TrimWhitespaces(propertyValue);
    }
    return propertyValue;
}

std::pair<std::string, std::string> ParseObjectSettings::GetNameValuePairFromProperty(const std::string line, const std::string openingChar, const std::string closingChar)
{
    std::string propertyName = GetName(line, openingChar, closingChar);
    std::string propertyValue = GetProperryValue(line, closingChar);
    return std::make_pair (propertyName, propertyValue);
}


std::unique_ptr<ParseObjectSettings::IParser> ParseObjectSettings::GetSettingParserForType(std::string settingTypeToParse){
  
    std::unique_ptr<IParser> result;
    RtUtilities::StringToLowercase(settingTypeToParse);

    if(settingTypeToParse == "object")
    {
        result = std::unique_ptr<IParser>(new ObjectParser());
    } 
    else if(settingTypeToParse == "material")
    {
        result = std::unique_ptr<IParser>(new MaterialParser());
    } 
    else if(settingTypeToParse == "background")
    {
        result = std::unique_ptr<IParser>(new BackgroundParser());
    } 
    else if(settingTypeToParse == "scene")
    {
        result = std::unique_ptr<IParser>(new SceneParser());
    } 
    else if(settingTypeToParse == "camera")
    {
        result = std::unique_ptr<IParser>(new CameraParser());
    } 
    else if(settingTypeToParse == "settings")
    {
        result = std::unique_ptr<IParser>(new RenderSettingsParser());
    } 
    else {
        result = std::unique_ptr<IParser>(nullptr);
    }  
    return result;
}

int ParseObjectSettings::GetSettingPriority(const std::string & setting){
  
    int result = 0;

    if(setting == "object")
    {
        result = 10;
    } 
    else if(setting == "material")
    {
        result = 5;
    } 
    else if(setting == "background")
    {
        result = 10;
    } 
    else if(setting == "camera")
    {
        result = 10;
    } 
    else if(setting == "scene")
    {
        result = 100;
    } 
    else if(setting == "settings"){
        result = 150;
    }
    else {
        result = 0;
    }  
    return result;
}