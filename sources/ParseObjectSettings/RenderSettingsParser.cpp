#include <iostream>
#include <memory>
#include "RenderSettingsParser.h"
#include "RtUtilities.h"
#include "ParsersHelperFunctions.h"
#include "ObjectManager.h"
#include "scene.h"


std::unique_ptr<ParseObjectSettings::Setting> ParseObjectSettings::RenderSettingsParser::ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors){

    std::unique_ptr<Setting> result = std::unique_ptr<Setting>(new RenderSetting());
    dynamic_cast<RenderSetting *>(result.get())->noErrorsOnParse = false;
    dynamic_cast<RenderSetting *>(result.get())->settingType = "settings";

    std::string propertyOpenSymbol = "[";
    std::string propertyCloseSymbol = "]:";

    bool convertSuccess = false;
    bool convertSuccessfulCummulative = true;
    std::string errorsConvert = "";

    for(std::string property : properties){

        std::string propertyName = GetName(property, propertyOpenSymbol, propertyCloseSymbol);
        std::string propertyValue = GetProperryValue(property, propertyCloseSymbol);

        RtUtilities::DebugLog("got property: '" + propertyName + "' \n", 3);

        if(propertyName == "name"){
            dynamic_cast<RenderSetting *>(result.get())->name = propertyValue;
        } 
        else if(propertyName == "scene"){
            dynamic_cast<RenderSetting *>(result.get())->sceneToRenderName = propertyValue;
        } 
        else if(propertyName == "camera"){
            dynamic_cast<RenderSetting *>(result.get())->cameraToRender = propertyValue;
        } 
        else if(propertyName == "width"){
            float propertyFloat = RtUtilities::ConvertPropertyToFloat(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<RenderSetting *>(result.get())->width = propertyFloat;
            } else {
                errorsConvert += "width; ";
            }
        }
        else if(propertyName == "height"){
            float propertyFloat = RtUtilities::ConvertPropertyToFloat(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<RenderSetting *>(result.get())->height = propertyFloat;
            } else {
                errorsConvert += "height; ";
            }
        }
        else if(propertyName == "samples"){
            int propertyInt = RtUtilities::ConvertPropertyToInt(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<RenderSetting *>(result.get())->samples = propertyInt;
                RtUtilities::DebugLog("got settings params samples: " + std::to_string(propertyInt), RtUtilities::LogColor::Grey);
                RtUtilities::DebugLog("\n", RtUtilities::LogColor::Grey);
            } else {
                errorsConvert += "samples; ";
            }
        }
        else if(propertyName == "bounces"){
            int propertyInt = RtUtilities::ConvertPropertyToInt(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<RenderSetting *>(result.get())->bounces = propertyInt;
                RtUtilities::DebugLog("got settings params bounces: " + std::to_string(propertyInt), 2, RtUtilities::LogColor::Grey);
                RtUtilities::DebugLog("\n", RtUtilities::LogColor::Grey);
            } else {
                errorsConvert += "bounces; ";
            }
        }

       convertSuccessfulCummulative = convertSuccessfulCummulative & convertSuccess;
    }
    
    if(convertSuccessfulCummulative)
    {
        dynamic_cast<RenderSetting *>(result.get())->noErrorsOnParse = true;
    } else {
        listOfErrors.push_back("Conversion of render settings '" + result.get()->name + "' failed because of mailformed properties: " + errorsConvert);
    }


    return result;
}

void ParseObjectSettings::RenderSettingsParser::ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors){
    RtUtilities::DebugLog("Setting render parameters: '" + settingToApply.get()->name + "'!\n", 2);

    RenderSetting * settingToWorkWith = dynamic_cast<RenderSetting *>(settingToApply.get());
 
    Object * sceneNameToCheck = ObjectManager::Instance()->GetObject(settingToWorkWith->sceneToRenderName);
    if(!sceneNameToCheck){
        RtUtilities::Log("[ERROR] Can't add a scene '" + settingToApply.get()->name + "' to render!\n",1, RtUtilities::LogColor::Red);
    } else {
        RtUtilities::DebugLog("Here will be render settings initialization later on!\n",1, RtUtilities::LogColor::White);
    }
  }

  void ParseObjectSettings::RenderSettingsParser::ApplyRenderSetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors, RenderParameters &renderParameters){
    RtUtilities::DebugLog("Setting render parameters: '" + settingToApply.get()->name + "'!\n", 2);

    RenderSetting * settingToWorkWith = dynamic_cast<RenderSetting *>(settingToApply.get());
 
    Object * sceneNameToCheck = ObjectManager::Instance()->GetObject(settingToWorkWith->sceneToRenderName);
    Object * cameraNameToCheck = ObjectManager::Instance()->GetObject(settingToWorkWith->cameraToRender);
    if(!sceneNameToCheck){
        RtUtilities::Log("[ERROR] Can't add a scene '" + settingToApply.get()->name + "' to render!\n",1, RtUtilities::LogColor::Red);
    } else if(!cameraNameToCheck) {
        RtUtilities::Log("[ERROR] Can't add a camera '" + settingToWorkWith->cameraToRender + "' to a scene!\n",1, RtUtilities::LogColor::Red);
    } else {
//        RtUtilities::DebugLog("Here will be render settings initialization later on!\n",1, RtUtilities::LogColor::White);
        renderParameters.width = settingToWorkWith->width;
        renderParameters.height = settingToWorkWith->height;
        renderParameters.samples = settingToWorkWith->samples;
        renderParameters.bounces = settingToWorkWith->bounces;
        renderParameters.sceneToRender = dynamic_cast<Scene *>(ObjectManager::Instance()->GetObject(settingToWorkWith->sceneToRenderName));
        
        Camera * cameraToAddTo = dynamic_cast<Camera *>(ObjectManager::Instance()->GetObject(settingToWorkWith->cameraToRender));
        cameraToAddTo->SetAspectRatio(settingToWorkWith->width, settingToWorkWith->height);
        renderParameters.cameraToRender = cameraToAddTo;
    }
  }