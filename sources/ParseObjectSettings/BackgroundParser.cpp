#include <iostream>
#include <memory>
#include "BackgroundParser.h"
#include "RtUtilities.h"
#include "ParsersHelperFunctions.h"
#include "Object.h"
#include "ObjectManager.h"
#include "SimpleGradientBackground.h"


std::unique_ptr<ParseObjectSettings::Setting> ParseObjectSettings::BackgroundParser::ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors){

    std::unique_ptr<Setting> result = std::unique_ptr<Setting>(new SimpleBackgroundSetting());
    result.get()->noErrorsOnParse = false;

    std::string propertyOpenSymbol = "[";
    std::string propertyCloseSymbol = "]:";

    bool convertSuccess = false;
    bool convertSuccessfulCummulative = true;
    std::string errorsConvert = "";


    for(std::string property : properties){

        std::pair <std::string, std::string> nameValuePairProperty = GetNameValuePairFromProperty(property, propertyOpenSymbol, propertyCloseSymbol);

        RtUtilities::DebugLog("got property: '" + nameValuePairProperty.first + "' \n", 3);

        if(nameValuePairProperty.first == "name"){
            dynamic_cast<SimpleBackgroundSetting *>(result.get())->name = nameValuePairProperty.second;
            convertSuccess = true;
        } 
        else if(nameValuePairProperty.first == "colorup"){
            dynamic_cast<SimpleBackgroundSetting *>(result.get())->colorUp = RtUtilities::ConvertPropertyToColor4(nameValuePairProperty.second, convertSuccess);
            if(!convertSuccess){
                errorsConvert += "colorup; ";
            }
        } 
        else if(nameValuePairProperty.first == "colorbottom"){
            dynamic_cast<SimpleBackgroundSetting *>(result.get())->colorBottom = RtUtilities::ConvertPropertyToColor4(nameValuePairProperty.second, convertSuccess);
            if(!convertSuccess){
                errorsConvert += "colorbottom; ";
            }
        };

        convertSuccessfulCummulative = convertSuccessfulCummulative & convertSuccess;
    }
    
    if(convertSuccessfulCummulative)
    {
        result.get()->noErrorsOnParse = true;
    } else {
        listOfErrors.push_back("Conversion of background '" + result.get()->name + "' failed because of mailformed properties: " + errorsConvert);
    }

    return result;
}

void ParseObjectSettings::BackgroundParser::ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors){
    RtUtilities::DebugLog("Background '" + settingToApply.get()->name + "' is here!\n", 2);
    Object * nameToCheck = ObjectManager::Instance()->GetObject(settingToApply.get()->name);
    if(nameToCheck){
        RtUtilities::Log("[ERROR] Can't create object '" + settingToApply.get()->name + "'\n", RtUtilities::LogColor::Red);
    } else {
        SimpleBackgroundSetting * backgroundToAdd = dynamic_cast<SimpleBackgroundSetting *>(settingToApply.get());
        IBackground *simpleBackground = new SimpleGradientBackground(
            backgroundToAdd->name,
            backgroundToAdd->colorBottom.toVec3(), 
            backgroundToAdd->colorUp.toVec3()
        );
    }
}
