#include <iostream>
#include <string>
#include "RtUtilities.h"
#include "CameraParser.h"
#include "ParsersHelperFunctions.h"
#include "ObjectManager.h"
#include "Camera.h"

std::unique_ptr<ParseObjectSettings::Setting> ParseObjectSettings::CameraParser::ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfconvertSuccesss){

//    std::vector<std::string> result = std::vector<std::string>();

    std::unique_ptr<Setting> result = std::unique_ptr<Setting>(new CameraSetting());
    dynamic_cast<CameraSetting *>(result.get())->noErrorsOnParse = false;
    dynamic_cast<CameraSetting *>(result.get())->settingType = "camera";

    std::string propertyOpenSymbol = "[";
    std::string propertyCloseSymbol = "]:";

    bool convertSuccess = false;
    bool convertSuccessfulCummulative = true;
    std::string errorsConvert = "";

    for(std::string property : properties){
//        std::cout << "------- o: " << property << "\n"; 
        std::string propertyName = GetName(property, propertyOpenSymbol, propertyCloseSymbol);

        std::string propertyValue = GetProperryValue(property, propertyCloseSymbol);

        if(propertyName == "name") {
            dynamic_cast<CameraSetting *>(result.get())->name = propertyValue;
            convertSuccess = true;
        } 
        else if(propertyName == "position") {
            vec3 propertyVec3 = RtUtilities::ConvertPropertyToVec3(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->position = propertyVec3;
            } else {
                errorsConvert += "position; ";
            }
        } 
        else if(propertyName == "rotation") {
            vec3 propertyVec3 = RtUtilities::ConvertPropertyToVec3(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->rotation = propertyVec3;
            } else {
                errorsConvert += "rotation; ";
            }
        } 
        else if(propertyName == "fov"){
            float propertyFloat = RtUtilities::ConvertPropertyToFloat(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->fov = propertyFloat;
            } else {
                errorsConvert += "fov; ";
            }
        }
        else if(propertyName == "focalpoint") {
            vec3 propertyVec3 = RtUtilities::ConvertPropertyToVec3(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->focalPoint = propertyVec3;
            } else {
                errorsConvert += "focalpoint; ";
            }
        } 
        else if(propertyName == "aperture"){
            float propertyFloat = RtUtilities::ConvertPropertyToFloat(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->apertureSize = propertyFloat;
            } else {
                errorsConvert += "aperture; ";
            }
        }
        else if(propertyName == "dof_enabled"){
            float propertyInt = RtUtilities::ConvertPropertyToInt(propertyValue, convertSuccess);
            if(convertSuccess){
                if(propertyInt == 1){
                    dynamic_cast<CameraSetting *>(result.get())->dofEnabled = true;
                } else{
                    dynamic_cast<CameraSetting *>(result.get())->dofEnabled = false;
                }
            } else {
                errorsConvert += "dof_enabled; ";
            }
        }
        else if(propertyName == "blades_enabled"){
            float propertyInt = RtUtilities::ConvertPropertyToInt(propertyValue, convertSuccess);
            if(convertSuccess){
                if(propertyInt == 1){
                    dynamic_cast<CameraSetting *>(result.get())->bladesEnabled = true;
                } else{
                    dynamic_cast<CameraSetting *>(result.get())->bladesEnabled = false;
                }
            } else {
                errorsConvert += "blades_enabled; ";
            }
        }
        else if(propertyName == "blades_number"){
            int propertyInt = RtUtilities::ConvertPropertyToInt(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->bladesNumber = propertyInt;
            } else {
                errorsConvert += "blades_number; ";
            }
        }
        else if(propertyName == "blades_rotation"){
            float propertyFloat = RtUtilities::ConvertPropertyToFloat(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<CameraSetting *>(result.get())->bladesRotation = propertyFloat;
            } else {
                errorsConvert += "blades_rotation; ";
            }
        }

        convertSuccessfulCummulative = convertSuccessfulCummulative & convertSuccess;
    }
    
    if(convertSuccessfulCummulative)
    {
        dynamic_cast<CameraSetting *>(result.get())->noErrorsOnParse = true;
    } else {
        listOfconvertSuccesss.push_back("Conversion of camera '" + result.get()->name + "' failed because of mailformed properties: " + errorsConvert);
    }

    return result;
}

void ParseObjectSettings::CameraParser::ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors){
    
    CameraSetting * settingToWorkWith =  dynamic_cast<CameraSetting *>(settingToApply.get());

    RtUtilities::DebugLog("Trying to create a camera: " + settingToWorkWith->name + "\n", RtUtilities::LogColor::Green);

    Camera * cameraToRender = new Camera(settingToWorkWith->name);
    cameraToRender->SetPosition(settingToWorkWith->position);
    cameraToRender->SetRotation(settingToWorkWith->rotation);
    cameraToRender->SetFOV(settingToWorkWith->fov);
    cameraToRender->SetFocalPoint(settingToWorkWith->focalPoint);
    cameraToRender->SetApertureSize(settingToWorkWith->apertureSize);
    cameraToRender->SetDofStatus(settingToWorkWith->dofEnabled);
    cameraToRender->SetBladesStatus(settingToWorkWith->bladesEnabled);
    if(settingToWorkWith)
        cameraToRender->SetBlades(settingToWorkWith->bladesNumber, settingToWorkWith->bladesRotation);
}

