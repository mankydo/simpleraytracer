#include <iostream>
#include <string>
#include "RtUtilities.h"
#include "ObjectParser.h"
#include "ParsersHelperFunctions.h"
#include "ObjectManager.h"
#include "sphere.h"
#include "plane.h"
#include "box.h"
#include "quad.h"
#include "Material.h"

std::unique_ptr<ParseObjectSettings::Setting> ParseObjectSettings::ObjectParser::ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfconvertSuccesss){

//    std::vector<std::string> result = std::vector<std::string>();

    std::unique_ptr<Setting> result = std::unique_ptr<Setting>(new ObjectSetting());
    dynamic_cast<ObjectSetting *>(result.get())->noErrorsOnParse = false;
    dynamic_cast<ObjectSetting *>(result.get())->settingType = "object";

    std::string propertyOpenSymbol = "[";
    std::string propertyCloseSymbol = "]:";

    bool convertSuccess = false;
    bool convertSuccessfulCummulative = true;
    std::string errorsConvert = "";

    for(std::string property : properties){
//        std::cout << "------- o: " << property << "\n"; 
        std::string propertyName = GetName(property, propertyOpenSymbol, propertyCloseSymbol);

        std::string propertyValue = GetProperryValue(property, propertyCloseSymbol);

        if(propertyName == "name") {
            dynamic_cast<ObjectSetting *>(result.get())->name = propertyValue;
            convertSuccess = true;
        } 
        else if(propertyName == "type") {
            dynamic_cast<ObjectSetting *>(result.get())->objectType = propertyValue;
            convertSuccess = true;
        }
        else if(propertyName == "position") {
            vec3 propertyVec3 = RtUtilities::ConvertPropertyToVec3(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<ObjectSetting *>(result.get())->position = propertyVec3;
            } else {
                errorsConvert += "position; ";
            }
        } 
        else if(propertyName == "rotation") {
            vec3 propertyVec3 = RtUtilities::ConvertPropertyToVec3(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<ObjectSetting *>(result.get())->rotation = propertyVec3 * ((2 * pi) / 360);
            } else {
                errorsConvert += "rotation; ";
            }
        }
        else if(propertyName == "material"){
            dynamic_cast<ObjectSetting *>(result.get())->materialName = propertyValue;
            convertSuccess = true;
        }
        else if(propertyName == "radius"){
            float propertyFloat = RtUtilities::ConvertPropertyToFloat(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<ObjectSetting *>(result.get())->raduis = propertyFloat;
            } else {
                errorsConvert += "radius; ";
            }
        }
        else if(propertyName == "dimensions") {
            vec3 propertyVec3 = RtUtilities::ConvertPropertyToVec3(propertyValue, convertSuccess);
            if(convertSuccess){
                dynamic_cast<ObjectSetting *>(result.get())->dimensions = propertyVec3;
            } else {
                errorsConvert += "dimensions; ";
            }
        } 

        convertSuccessfulCummulative = convertSuccessfulCummulative & convertSuccess;
    }
    
    if(convertSuccessfulCummulative)
    {
        dynamic_cast<ObjectSetting *>(result.get())->noErrorsOnParse = true;
    } else {
        listOfconvertSuccesss.push_back("Conversion of object '" + result.get()->name + "' failed because of mailformed properties: " + errorsConvert);
    }

    return result;
}

void ParseObjectSettings::ObjectParser::ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors){
    
    ObjectSetting * settingToWorkWith =  dynamic_cast<ObjectSetting *>(settingToApply.get());

    RtUtilities::DebugLog("Trying to create an object: " + settingToWorkWith->name + "\n", RtUtilities::LogColor::Green);

    Material * materialToUse = dynamic_cast<Material *>(ObjectManager::Instance()->GetObject(settingToWorkWith->materialName));
    //check if the material can be created and give a warning about it

    SceneObject * objectToCreate;

    if(settingToWorkWith->objectType == "sphere") {
        objectToCreate = new sphere(settingToWorkWith->raduis, settingToWorkWith->name, materialToUse);
    }
    else if(settingToWorkWith->objectType == "plane"){
        objectToCreate = new plane(settingToWorkWith->name, materialToUse);
    }
    else if(settingToWorkWith->objectType == "box"){
       objectToCreate = new box(settingToWorkWith->dimensions, settingToWorkWith->name, materialToUse);
    }
    else if(settingToWorkWith->objectType == "xy-quad"){
       objectToCreate = new quad(settingToWorkWith->dimensions, settingToWorkWith->name, materialToUse);
    }
    else {
        return;
    }
    
    objectToCreate->SetPosition(settingToWorkWith->position);
    objectToCreate->SetRotation(settingToWorkWith->rotation);

}

