#include <iostream>
#include <memory>
#include "MaterialParser.h"
#include "RtUtilities.h"
#include "ParsersHelperFunctions.h"
#include "ObjectManager.h"
#include "Materials.h"


std::unique_ptr<ParseObjectSettings::Setting> ParseObjectSettings::MaterialParser::ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors){

    std::unique_ptr<Setting> result = std::unique_ptr<Setting>(new MaterialSetting());
    result.get()->noErrorsOnParse = false;

    std::string propertyOpenSymbol = "[";
    std::string propertyCloseSymbol = "]:";
    bool convertSuccessful = false;
    bool convertSuccessfulCummulative = true;
    std::string errorsConvert = "";

    for(std::string property : properties){

        std::pair <std::string, std::string> nameValuePairProperty = GetNameValuePairFromProperty(property, propertyOpenSymbol, propertyCloseSymbol);

        RtUtilities::DebugLog("got property: '" + nameValuePairProperty.first + "' \n", 3);

        if(nameValuePairProperty.first == "name"){
            dynamic_cast<MaterialSetting *>(result.get())->name = nameValuePairProperty.second;
            convertSuccessful = true;
        } 
        else if(nameValuePairProperty.first == "type"){
            dynamic_cast<MaterialSetting *>(result.get())->materialType = nameValuePairProperty.second;
            RtUtilities::DebugLog("got type: '" + nameValuePairProperty.second + "' \n", 4);

            convertSuccessful = true;
        } 
        else if(nameValuePairProperty.first == "roughness"){
            dynamic_cast<MaterialSetting *>(result.get())->materialRoughness = RtUtilities::ConvertPropertyToFloat(nameValuePairProperty.second, convertSuccessful);
            if(!convertSuccessful){
                errorsConvert += "roughness; ";
            }
        } 
        else if(nameValuePairProperty.first == "color"){
            dynamic_cast<MaterialSetting *>(result.get())->materialColor = RtUtilities::ConvertPropertyToColor4(nameValuePairProperty.second, convertSuccessful);
            if(!convertSuccessful){
                errorsConvert += "color; ";
            }
        };

        convertSuccessfulCummulative = convertSuccessfulCummulative & convertSuccessful;
    }
    
    if(convertSuccessfulCummulative)
    {
        result.get()->noErrorsOnParse = true;
    } else {
        listOfErrors.push_back("Conversion of material '" + result.get()->name + "' failed because of mailformed properties: " + errorsConvert);
    }
    return result;
}

void ParseObjectSettings::MaterialParser::ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors){
    RtUtilities::DebugLog("material '" + settingToApply.get()->name + "' is here!\n", 2);

    MaterialSetting * settingToWorkWith = dynamic_cast<MaterialSetting *>(settingToApply.get());
    Object * materialNameToCheck = ObjectManager::Instance()->GetObject(settingToWorkWith->name);

    if(materialNameToCheck){
        RtUtilities::Log("[ERROR] Can't create object '" + settingToApply.get()->name + "'\n",1, RtUtilities::LogColor::Red);
    } 
    else {
        if(settingToWorkWith->materialType == "lambert") {
            Material * lambert = new MaterialLambert(settingToWorkWith->name, settingToWorkWith->materialColor);
        } 
        else if (settingToWorkWith->materialType == "metallic") {
            Material * metallic = new MaterialMetallic(settingToWorkWith->name, settingToWorkWith->materialColor, settingToWorkWith->materialRoughness);
        } 
        else {
            RtUtilities::Log("[ERROR] Can't create material '" + settingToApply.get()->name + " due to unknown material type '" + settingToWorkWith->materialType + "'\n",1, RtUtilities::LogColor::Red);
        }
    }
}