#include <iostream>
#include <memory>
#include "SceneParser.h"
#include "RtUtilities.h"
#include "ParsersHelperFunctions.h"
#include "ObjectManager.h"
#include "scene.h"
#include "IBackground.h"


std::unique_ptr<ParseObjectSettings::Setting> ParseObjectSettings::SceneParser::ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors){

    std::unique_ptr<Setting> result = std::unique_ptr<Setting>(new SceneSetting());
    std::string propertyOpenSymbol = "[";
    std::string propertyCloseSymbol = "]:";

    for(std::string property : properties){

        std::pair <std::string, std::string> nameValuePairProperty = GetNameValuePairFromProperty(property, propertyOpenSymbol, propertyCloseSymbol);

        RtUtilities::DebugLog("got property: '" + nameValuePairProperty.first + "' \n", 3);

        if(nameValuePairProperty.first == "name"){
            dynamic_cast<SceneSetting *>(result.get())->name = nameValuePairProperty.second;
        } 

        if(nameValuePairProperty.first == "background"){
            dynamic_cast<SceneSetting *>(result.get())->backgroundName = nameValuePairProperty.second;
        } 
        else if(nameValuePairProperty.first == "addobject"){
            dynamic_cast<SceneSetting *>(result.get())->objectsToAdd.push_back(nameValuePairProperty.second);
        };

    }
    
    result.get()->noErrorsOnParse = true;

    return result;
}

void ParseObjectSettings::SceneParser::ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors){
    RtUtilities::DebugLog("Creating a scene '" + settingToApply.get()->name + "'!\n", 2);

    SceneSetting * settingToWorkWith = dynamic_cast<SceneSetting *>(settingToApply.get());
 
    Object * backgroundToCheck = ObjectManager::Instance()->GetObject(settingToWorkWith->backgroundName);
    if(!backgroundToCheck){
        RtUtilities::Log("[ERROR] Can't create scene '" + settingToApply.get()->name + "' "
        + "because there is no background '" + settingToWorkWith->backgroundName + "' exists!" + "\n", 1, RtUtilities::LogColor::Red);
    } 
    else {
        Object * sceneNameToCheck = ObjectManager::Instance()->GetObject(settingToWorkWith->name);
        if(sceneNameToCheck){
            RtUtilities::Log("[ERROR] Can't create object '" + settingToApply.get()->name + "'\n",1, RtUtilities::LogColor::Red);
        } else {
            Scene * sceneToCreate = new Scene(settingToWorkWith->name, dynamic_cast<IBackground *>(backgroundToCheck));
            //create objects
            for(auto objectToAddName : settingToWorkWith->objectsToAdd){
                    SceneObject * objectToAdd = dynamic_cast<SceneObject *>(ObjectManager::Instance()->GetObject(objectToAddName));
                    if(objectToAdd){
                        sceneToCreate->addObject(objectToAdd);                      
                    } else {
                        RtUtilities::Log("[ERROR] '" + settingToWorkWith->name + "': Can't add object '" + objectToAddName + "'\n",1, RtUtilities::LogColor::Red);
                    }
            }
        }
    }
  }