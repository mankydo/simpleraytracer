#ifndef SCENEPARCER_H
#define SCENEPARCER_H

#include <vector>
#include <string>
#include "IParser.h"

namespace ParseObjectSettings {

    class SceneSetting: public Setting
    {
    public:
        std::string backgroundName = "";
        std::vector<std::string> objectsToAdd = std::vector<std::string>();
    };

    class SceneParser: public IParser
    {
    public:
        std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) override;
        void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) override;
    };

}
#endif