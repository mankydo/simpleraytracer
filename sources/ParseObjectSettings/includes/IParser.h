#ifndef IPARSER_H
#define IPARSER_H

#include <vector>
#include <string>
#include <memory>
#include "Setting.h"

namespace ParseObjectSettings {

    class IParser
    {
    public:
        virtual std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) = 0;
        virtual void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) = 0;
        virtual ~IParser(){};
    };

}
#endif