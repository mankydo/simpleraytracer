#ifndef PARSERHELPERS_H
#define PARSERHELPERS_H

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <utility>
#include "IParser.h"
#include "vec3.h"
#include "color4.h"

namespace ParseObjectSettings {

    std::string GetName(const std::string line, const std::string openingChar, const std::string closingChar);
    std::string GetProperryValue(const std::string line, const std::string closingChar);
    std::pair<std::string, std::string> GetNameValuePairFromProperty(const std::string line, const std::string openingChar, const std::string closingChar);
    std::unique_ptr<IParser> GetSettingParserForType(std::string settingTypeToParse);
    int GetSettingPriority(const std::string & setting);

} 

#endif