#ifndef MATERIALPARSER_H
#define MATERIALPARSER_H

#include <vector>
#include <string>
#include "IParser.h"
#include "color4.h"

namespace ParseObjectSettings {

    class MaterialSetting: public Setting
    {
    public:
        std::string materialType = "";
        color4 materialColor = color4();
        float materialRoughness = 1;
    };
    class MaterialParser: public IParser
    {
    public:
        std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) override;
        void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) override;
    };

}
#endif