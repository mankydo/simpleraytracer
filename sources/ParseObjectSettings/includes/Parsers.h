#ifndef PARSERS_H
#define PARSERS_H

#include <string>
#include <vector>
#include <map>

#include "IRenderer.h"
#include "ObjectParser.h"
#include "MaterialParser.h"
#include "BackgroundParser.h"
#include "SceneParser.h"
#include "CameraParser.h"


namespace ParseObjectSettings {

    struct UnparsedSettings
    {
        std::string settingName;
        std::vector<std::string> properties;
    };
    

    void ParseAndApplyFileSettings(const std::string filename, std::vector<std::string> &errorLog, RenderParameters &parsedRenderParameters);

    std::vector<ParseObjectSettings::UnparsedSettings> ParseSettings(std::vector<std::string> &settingsToParse);
    std::vector<std::string> ReadSettingsFile(const std::string &filename);

//    enum SettingType {Object, Material, Background, Scene, Undefined};
}

#endif