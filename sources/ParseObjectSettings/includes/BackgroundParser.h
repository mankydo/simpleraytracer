#ifndef BACKGROUNDPARSER_H
#define BACKGROUNDPARSER_H

#include <vector>
#include <string>
#include "IParser.h"
#include "color4.h"

namespace ParseObjectSettings {

    class SimpleBackgroundSetting: public Setting
    {
    public:
        color4 colorUp = color4();
        color4 colorBottom = color4();
    };

    class BackgroundParser: public IParser
    {
    public:
        std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) override;
        void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) override;
    };

}
#endif