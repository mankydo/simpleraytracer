#ifndef OBJECTPARSER_H
#define OBJECTPARSER_H

#include <vector>
#include <string>
#include "IParser.h"
#include "Setting.h"
#include "vec3.h"

namespace ParseObjectSettings {

    class ObjectSetting: public Setting
    {
    public:
        std::string objectType = "";
        vec3 position = vec3();
        vec3 rotation = vec3(0);
        std::string materialName = "";
        float raduis = 0.0;
        vec3 dimensions;
    };

    class ObjectParser: public IParser
    {
    public:
        std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) override;
        void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) override;
    };

}
#endif