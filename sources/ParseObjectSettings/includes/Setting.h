#ifndef BASESETTING_H
#define BASESETTING_H

#include <string>
#include <memory>

namespace ParseObjectSettings {

    class Setting
    {
    public:
        std::string name = "";
        std::string settingType = "";
        int priority = 10;
        bool noErrorsOnParse = false;

        static bool ComparePrioritiesUniquePtrs(const std::unique_ptr<ParseObjectSettings::Setting> & a, const std::unique_ptr<ParseObjectSettings::Setting> & b);
        static bool ComparePriorities(const ParseObjectSettings::Setting * a, const ParseObjectSettings::Setting * b);
        virtual ~Setting(){};
    };

}
#endif