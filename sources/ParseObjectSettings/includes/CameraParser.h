#ifndef CAMERAPARSER_H
#define CAMERAPARSER_H

#include <vector>
#include <string>
#include "IParser.h"
#include "Setting.h"
#include "vec3.h"

namespace ParseObjectSettings {

    class CameraSetting: public Setting
    {
    public:
        vec3 position = vec3();
        vec3 rotation = vec3();
        float fov = 80.0;

        vec3 focalPoint = vec3();
        float apertureSize = .3;
        bool dofEnabled = false;

        bool bladesEnabled = false;
        int bladesNumber = 6;
        float bladesRotation = .2;
    };

    class CameraParser: public IParser
    {
    public:
        std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) override;
        void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) override;
    };

}
#endif