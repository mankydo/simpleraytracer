#ifndef RENDERSETTINGSPARCER_H
#define RENDERSETTINGSPARCER_H

#include <vector>
#include <string>
#include "IParser.h"
#include "IRenderer.h"

namespace ParseObjectSettings {

    class RenderSetting: public Setting
    {
    public:
        std::string sceneToRenderName = "";
        std::string cameraToRender = "";
        float width = 200;
        float height = 100;
        int samples = 10;
        int bounces = 10;
    };

    class RenderSettingsParser: public IParser
    {
    public:
        std::unique_ptr<Setting> ParseSetting(const std::vector<std::string> & properties, std::vector<std::string> & listOfErrors) override;
        void ApplySetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors) override;
        void ApplyRenderSetting(const std::unique_ptr<Setting> & settingToApply, std::vector<std::string> & listOfErrors, RenderParameters &renderParameters);
    };

}
#endif