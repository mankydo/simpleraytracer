#include <fstream>
#include <iostream>
#include <algorithm>
#include "Parsers.h"
#include "ParsersHelperFunctions.h"
#include "RtUtilities.h"
#include "Setting.h"
#include "RenderSettingsParser.h"


void ParseObjectSettings::ParseAndApplyFileSettings(const std::string filename, std::vector<std::string> &errorLog, RenderParameters &parsedRenderParameters){

    std::vector<std::string> settingsUnparsed = ReadSettingsFile(filename);

    if(settingsUnparsed.size() == 0){
        errorLog.push_back("[ERROR] File " + filename + " cannot be found.");
    } else {

//        std::map<std::string, std::vector<std::string>> listOfUnparsedSettings = ParseSettings(settingsUnparsed);
        std::vector<UnparsedSettings> listOfUnparsedSettings = ParseSettings(settingsUnparsed);

        std::vector<std::unique_ptr<Setting>> ListOfSettingsParsed = std::vector<std::unique_ptr<Setting>>();

        for(auto setting: listOfUnparsedSettings)
        {
            RtUtilities::DebugLog("name of the setting is: ", RtUtilities::Green);
            RtUtilities::DebugLog(setting.settingName + "\n", RtUtilities::Default);

            std::unique_ptr<IParser> parserWorker = GetSettingParserForType(setting.settingName);
            if(parserWorker)
            {
                ListOfSettingsParsed.push_back(parserWorker->ParseSetting(setting.properties, errorLog));
                if(!ListOfSettingsParsed.empty()){
                    ListOfSettingsParsed.back().get()->settingType = setting.settingName;
                    ListOfSettingsParsed.back().get()->priority = GetSettingPriority(setting.settingName);
                }

            } else {
                RtUtilities::DebugLog("\nCannot create Parser Worker for setting named: " + setting.settingName + "\n\n", RtUtilities::Red);
            }
        }

        ListOfSettingsParsed.erase(std::remove_if(
                ListOfSettingsParsed.begin(), ListOfSettingsParsed.end(), 
                [](const std::unique_ptr<Setting>& x){
                    return !x.get()->noErrorsOnParse;
                }),
            ListOfSettingsParsed.end()
        );
        ListOfSettingsParsed.shrink_to_fit();
        std::sort(ListOfSettingsParsed.begin(),ListOfSettingsParsed.end(),Setting::ComparePrioritiesUniquePtrs);

        for(std::size_t i = 0; i != ListOfSettingsParsed.size(); i++){

            RtUtilities::DebugLog("setting name is: " + ListOfSettingsParsed[i].get()->name 
                + " and it's type is: " + ListOfSettingsParsed[i].get()->settingType 
                + "; priority: " + std::to_string(ListOfSettingsParsed[i].get()->priority) + "\n"
                , RtUtilities::LogColor::Cyan
            );

            std::unique_ptr<IParser> parserWorker = GetSettingParserForType(ListOfSettingsParsed[i].get()->settingType);
            if(parserWorker)
            {
                parserWorker->ApplySetting(ListOfSettingsParsed[i], errorLog);
            }

            if(ListOfSettingsParsed[i].get()->settingType == "settings"){
//                sceneToRenderName = dynamic_cast<RenderSetting *>(ListOfSettingsParsed[i].get())->sceneToRenderName;               
                dynamic_cast<RenderSettingsParser *>(parserWorker.get())->ApplyRenderSetting(ListOfSettingsParsed[i], errorLog, parsedRenderParameters);
            }
        }

        RtUtilities::Log("\n");

    }
}

std::vector<ParseObjectSettings::UnparsedSettings> ParseObjectSettings::ParseSettings(std::vector<std::string> &settingsToParse){

//    std::map<std::string, std::vector<std::string>> settingsOut = std::map<std::string, std::vector<std::string>>();
    std::vector<ParseObjectSettings::UnparsedSettings> settingsOut = std::vector<ParseObjectSettings::UnparsedSettings>();
    
    std::string currentSettingName = "";

    for(std::string setting : settingsToParse)
    {
        if(setting.find("//") == std::string::npos){
            std::string settingName = ParseObjectSettings::GetName(setting, "<", ">");
            if(settingName != "-1")
            {
                if(settingName.substr(0, 1) != "/")
                {
                    currentSettingName = settingName;
                    settingsOut.push_back({currentSettingName, std::vector<std::string>()});
                } else {
                    currentSettingName = "";
                }
            } else {
                if(currentSettingName != "") 
                {
                    settingsOut.back().properties.push_back(setting);
                }
            }
        }
    }
    return settingsOut;
}

std::vector<std::string> ParseObjectSettings::ReadSettingsFile(const std::string &filename){
    std::fstream settingsFile;
    std::vector<std::string> settings = std::vector<std::string>();

    std::ifstream ifile(filename);
    if(ifile){
        settingsFile.open(filename, std::ios::in);
        if(settingsFile.is_open()){
            std::string newLine;
            while(std::getline(settingsFile, newLine)){
                if(newLine.substr(0,2) != "//" && newLine.substr(0,1) != "#")
                {
                    settings.push_back(newLine);
                }
            }
        }
    }
    return settings;
}
