#include "quad.h"
#include "RtMath.h"
#include "RtUtilities.h"


quad::quad(vec3 in_dimensions, std::string name, IMaterial * material): SceneObject(name)
{
    this->normal = unit_vector(vec3(0,0,-1));
    this->SetBounds(in_dimensions);
    this->_MaterialPointer = material;
}

vec3 quad::getOrigin(){
    return this->origin;
}
vec3 quad::getNormal(){
    return this->normal;
}

void quad::GetBounds(vec3 &outBoundsMin, vec3 &outBoundsMax) const{
    outBoundsMin = this->boundsMin;
    outBoundsMax = this->boundsMax;
}
vec3 quad::GetBoundsMin() const{
    return this->boundsMin;
}
vec3 quad::GetBoundsMax() const{
    return this->boundsMax;
}

void quad::SetBounds(vec3 in_dimensions){
    this->dimensions = in_dimensions;
    this->boundsMin = this->origin - (in_dimensions / 2);
    this->boundsMax = this->origin + (in_dimensions / 2);
}

IMaterial * quad::GetMaterial() const{
    return _MaterialPointer;
}

bool quad::Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const{
    bool result = false;

    double hit_result = 0;
    vec3 objOrigin = vec3(0);

/* //    if(this->normal.z() * in_ray.direction().z() >= 0 ) 
//  strict equality is for double sided quad
    if(this->normal.z() * in_ray.direction().z() == 0 ) 
    {
        result = false;
    } 
    else {
   
    //get x from xz-intersection where z = 0

        float xBounds = this->dimensions.x()/2;

        float xP = in_ray.origin().x() - ( in_ray.direction().x() / in_ray.direction().z() ) * in_ray.origin().z();

//        if( std::min( xBounds, std::max(-xBounds, xP)) == xP) {
        if(xP > -xBounds && xP < xBounds) {

        //get y from yz-intersection where z = 0

            float yBounds = this->dimensions.y()/2;

            float yP = in_ray.origin().y() - ( in_ray.direction().y() / in_ray.direction().z() ) * in_ray.origin().z();
    
//            if(std::min( yBounds, std::max(-yBounds, yP)) == yP) {
            if(yP > -yBounds && yP < yBounds) {

            //get intersection point P from (x,y,0)

                vec3 hitPoint = vec3(xP, yP, 0);

            //get distance on a ray from magnitude of a vector (RayOrigin - P)
                hit_result = (in_ray.origin() - hitPoint).length();
                result = true;
            }
        }
    } */

    float xBounds = this->dimensions.x()/2;
    float yBounds = this->dimensions.y()/2;

    result = RtMath::XYQuadRayIntersection(in_ray, -xBounds, xBounds, -yBounds, yBounds, hit_result);

/*     
    //for testing purposes the method using generic plane intersection with limiting resulting x and y

    result = RtMath::PlaneRayIntersection(objOrigin, this->normal, in_ray, hit_result);
    
    vec3 hitPoint = in_ray.point_at_parameter(hit_result);
    float xBounds = this->dimensions.x()/2;
    float yBounds = this->dimensions.y()/2;
    if( std::min( xBounds, std::max(-xBounds, hitPoint.x())) != hitPoint.x() || 
        std::min( yBounds, std::max(-yBounds, hitPoint.y())) != hitPoint.y()) 
    {
        result = false;
    } */


    if(result) {

        output_info.materialPointer = this->GetMaterial();       
       
        if(hit_result < t_max && hit_result > t_min){
            output_info.t = hit_result;
            output_info.normal = unit_vector(this->normal);
            output_info.setFaceNormal(in_ray, output_info.normal);    
            result = true;      
        } else {
            result = false;
        }
    } else {
        result = false;
    } 
    return result;
}

