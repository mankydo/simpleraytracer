#include <math.h>
#include "box.h"
#include "RtMath.h"
#include "RtUtilities.h"


box::box(vec3 in_dimensions, std::string name, IMaterial * material): SceneObject(name)
{
    this->origin = vec3(0);
    this->SetBounds(in_dimensions);
    this->_MaterialPointer = material;
}

vec3 box::getOrigin() const{
    return this->origin;
}
void box::GetBounds(vec3 &outBoundsMin, vec3 &outBoundsMax) const{
    outBoundsMin = this->boundsMin;
    outBoundsMax = this->boundsMax;
}
vec3 box::GetBoundsMin() const{
    return this->boundsMin;
}
vec3 box::GetBoundsMax() const{
    return this->boundsMax;
}

void box::SetBounds(vec3 in_dimensions){
    this->dimensions = in_dimensions;
    this->boundsMin = this->origin - (in_dimensions / 2);
    this->boundsMax = this->origin + (in_dimensions / 2);

    RtUtilities::DebugLog(
        "bounds min: "
        + std::to_string(this->boundsMin.x()) 
        + ":" + std::to_string(this->boundsMin.y())
        + ":" + std::to_string(this->boundsMin.z())
        +"\n"
    );

    RtUtilities::DebugLog(
        "bounds max: "
        + std::to_string(this->boundsMax.x()) 
        + ":" + std::to_string(this->boundsMax.y())
        + ":" + std::to_string(this->boundsMax.z())
        +"\n"
    );

}

IMaterial * box::GetMaterial() const{
    return _MaterialPointer;
}

bool box::Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const{
    bool result = false;
    vec3 hitPoint;
    double hit_result = 0;
    vec3 planeNormalToCheck;
    vec3 planeOrigin;
//    vec3 objPosition = this->Position();
//    vec3 objPosition = vec3(0);

    planeNormalToCheck = vec3(-1,0,0);
    planeOrigin = this->GetBoundsMin();

    if(in_ray.direction().x() < 0){
        planeNormalToCheck *= -1;
        planeOrigin = this->GetBoundsMax();
    }

    bool resultX = RtMath::PlaneRayIntersection(planeOrigin, planeNormalToCheck, in_ray, hit_result);
    if(resultX){
        hitPoint = in_ray.point_at_parameter(hit_result);

        resultX = 
            hitPoint.z() > this->boundsMin.z() 
            && hitPoint.z() < this->boundsMax.z() 
            && hitPoint.y() > this->boundsMin.y() 
            && hitPoint.y() < this->boundsMax.y();
    }
    if(resultX){
        output_info.t = hit_result;
        output_info.normal = planeNormalToCheck;
    }

/* //    sign = -1 *( (in_ray.direction().y() > 0) - (in_ray.direction().y() < 0));
    sign = -1 * std::copysignf(1, in_ray.direction().y());
    planeNormalToCheck = vec3( 0, 1 * sign, 0);
    planeOrigin = this->position + halfDimension * sign; */
    
    planeNormalToCheck = vec3(0,1,0);
    planeOrigin = this->GetBoundsMax();

    if(in_ray.direction().y() > 0){
        planeNormalToCheck *= -1;
        planeOrigin = this->GetBoundsMin();
    }

    bool resultY = RtMath::PlaneRayIntersection(planeOrigin, planeNormalToCheck, in_ray, hit_result);
    if( resultY){
        hitPoint = in_ray.point_at_parameter(hit_result);

        resultY = hitPoint.x() > this->boundsMin.x() 
            && hitPoint.x() < this->boundsMax.x() 
            && hitPoint.z() > this->boundsMin.z()
            && hitPoint.z() < this->boundsMax.z();
    }
    if(resultY){
        output_info.t = hit_result;
        output_info.normal = planeNormalToCheck;
    }

/* //    sign = (in_ray.direction().z() > 0) - (in_ray.direction().z() < 0);
    sign = std::copysignf(1, in_ray.direction().z());
    planeNormalToCheck = vec3(0,0, 1 * sign);
    planeOrigin = this->position - halfDimension * sign; */

    planeNormalToCheck = vec3(0,0,-1);
    planeOrigin = this->GetBoundsMin();

    if(in_ray.direction().z() < 0){
        planeNormalToCheck *= -1;
        planeOrigin = this->GetBoundsMax();
    }

    bool resultZ = RtMath::PlaneRayIntersection(planeOrigin, planeNormalToCheck, in_ray, hit_result);
    if( resultZ){
        hitPoint = in_ray.point_at_parameter(hit_result);

        resultZ = hitPoint.x() > this->boundsMin.x() 
            && hitPoint.x() < this->boundsMax.x() 
            && hitPoint.y() > this->boundsMin.y() 
            && hitPoint.y() < this->boundsMax.y();
    }
    if(resultZ){
        output_info.t = hit_result;
        output_info.normal = planeNormalToCheck;
    }

    result = resultX | resultY | resultZ;

    if(result){
        if(output_info.t < t_max && output_info.t > t_min){
            output_info.materialPointer = this->GetMaterial();
            result = true;      
        } else {
            result = false;
        }        
        return result;
    }

    return false;
}