#ifndef PLANE_H
#define PLANE_H

#include "IHittable.h"
#include "SceneObject.h"

class plane: public IHittable, public SceneObject
{
private:
    vec3 origin;
    vec3 normal;
//    std::shared_ptr<IMaterial> _MaterialPointer;
    IMaterial * _MaterialPointer;

public:
    plane(std::string name, IMaterial * material);
    bool Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const;
    vec3 getOrigin();
    vec3 getNormal();
    IMaterial * GetMaterial() const;
};

#endif