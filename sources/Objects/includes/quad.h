#ifndef QUAD_H
#define QUAD_H

#include "IHittable.h"
#include "SceneObject.h"

class quad: public IHittable, public SceneObject
{
private:
    vec3 origin;
    vec3 normal;
    vec3 dimensions;
    vec3 boundsMin;
    vec3 boundsMax;
    IMaterial * _MaterialPointer;

public:
    quad(vec3 in_dimensions, std::string name, IMaterial * material);
    bool Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const override;
    vec3 getOrigin();
    vec3 getNormal();

    void GetBounds(vec3 &outBoundsMin, vec3 &outBoundsMax) const;
    vec3 GetBoundsMin() const;
    vec3 GetBoundsMax() const;
    void SetBounds(vec3 in_dimensions);

    IMaterial * GetMaterial() const;
};

#endif