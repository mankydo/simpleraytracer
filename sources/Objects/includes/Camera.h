#ifndef CAMERA_H
#define CAMERA_H

#include <string>
#include "Ray.h"
#include "SceneObject.h"

class Camera : public SceneObject
{
    private:
        vec3 origin = vec3(0.0, 0.0, 0.0);
        vec3 lower_left_corner = vec3(-2.0, -1.0, -1.0);
        vec3 horizontalOffset = vec3(4.0, 0.0, 0.0);
        vec3 verticalOffset = vec3(0.0, 2.0, 0.0);
        float frustumLength = 1;
        float fovDegrees = 90;
        int internalWidth = 200;
        int internalHeight = 100;

        vec3 focalPoint = vec3(0, 0, -1);
        float apertureSize = .3;
        bool dofEnabled = false;

        bool bladesEnabled = false;
        int bladesNumber = 6;
        float bladesRotation = .2;

    public:

        Camera(std::string name);
        Camera(std::string name, int width, int height, float fieldOfViewAngle);

        void SetFOV(float FieldOfViewHorizontalAngle);
        void SetAspectRatio(float widthInPixels, float heightInPixels); 

        ray get_ray(float u, float v) const;

        void SetPosition(vec3 position) override;
        void SetRotation(vec3 rotation) override;
        void SetScale(vec3 scale) override {};

        vec3 GetFocalPoint() const;
        float GetApertureSize() const;
        bool GetDofStatus() const;
        void SetFocalPoint(vec3 focalPointToSet);
        void SetApertureSize(float apertureSizeToSet);
        void SetDofStatus(bool dofStatus);

        bool GetBladesStatus();
        int GetBladesNumber();
        float GetBladesRotation();
        void SetBladesStatus(bool bladesEnabled);
        void SetBlades(int bladesNumber, float bladesRotation);
};

#endif 