#ifndef SPHERE_H
#define SPHERE_H

#include "IHittable.h"
#include "SceneObject.h"

class sphere: public IHittable, public SceneObject
{
private:
    vec3 origin = vec3(0);
    float radius;
//    std::shared_ptr<IMaterial> _MaterialPointer;
    IMaterial * _MaterialPointer;

public:
    sphere(float in_raduis, std::string name, IMaterial * material);
    bool Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const;
    vec3 getOrigin();
    float getRadius();
    IMaterial * GetMaterial() const;

    void SetScale(vec3 scale) override;

};

#endif