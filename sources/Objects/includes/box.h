#ifndef BOX3D_H
#define BOX3D_H

#include "IHittable.h"
#include "SceneObject.h"

class box: public IHittable, public SceneObject
{
private:
    vec3 origin;
    vec3 dimensions;
    vec3 boundsMin;
    vec3 boundsMax;
    bool branchless = false;
    IMaterial * _MaterialPointer;

public:
    box(vec3 in_dimensions, std::string name, IMaterial * material);
    bool Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const;
    vec3 getOrigin() const;
    IMaterial * GetMaterial() const;

    void GetBounds(vec3 &outBoundsMin, vec3 &outBoundsMax) const;
    vec3 GetBoundsMin() const;
    vec3 GetBoundsMax() const;
    void SetBounds(vec3 in_dimensions);
};

#endif