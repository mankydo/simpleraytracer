#include "sphere.h"


sphere::sphere(float in_raduis, std::string name, IMaterial * material): SceneObject(name)
{
    this->radius = in_raduis;
    this->_MaterialPointer = material;
}

vec3 sphere::getOrigin(){
    return this->origin;
}
float sphere::getRadius(){
    return this->radius;
}
void sphere::SetScale(vec3 scale){
    this->radius *= scale.x();
}


IMaterial * sphere::GetMaterial() const{
    return _MaterialPointer;
}

bool sphere::Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const{
    bool result = false;

//    vec3 objOrigin = this->Position();
    vec3 objOrigin = vec3(0);

    vec3 oc = in_ray.origin() - objOrigin;
    float a = dot(in_ray.direction(), in_ray.direction());
    float b = 2.0 * dot(oc, in_ray.direction());
    float c = dot(oc, oc) - this->radius * this->radius;
    float discriminant = (b * b) - (4.0 * a * c);
    if(discriminant > 0) {

        output_info.materialPointer = this->GetMaterial();
        
        float hit_result = (-b - sqrt(discriminant)) / (2.0 * a);
        
        if(hit_result < t_max && hit_result > t_min){
            output_info.t = hit_result;
            output_info.p = in_ray.point_at_parameter(output_info.t);
            output_info.normal = unit_vector((output_info.p - objOrigin) / this->radius);
            output_info.setFaceNormal(in_ray, output_info.normal);
            result = true;      
        } else {
             hit_result = (-b + sqrt(discriminant)) / (2.0 * a);
            if(hit_result < t_max && hit_result > t_min){
                output_info.t = hit_result;
                output_info.p = in_ray.point_at_parameter(output_info.t);
                output_info.normal = unit_vector((output_info.p - objOrigin) / this->radius);
                output_info.setFaceNormal(in_ray, output_info.normal);
                result = true;      
            }
        }
    } else {
        result = false;
    }
    return result;
}

