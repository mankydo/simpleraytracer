#include "Camera.h"
#include <cmath>
//#include <string>
#include "RtMath.h"
#include "RtUtilities.h"
#include "matrix3x3.h"
#include "matrix4x4.h"

Camera::Camera(std::string name): SceneObject(name)
{
        float horisontalWidth = 400;
        float verticalHeight = 200;
        float fov = 90;

        this->SetObjectType(ObjectTypes::camerasObjects);

        this->SetFOV(fov);
        this->SetAspectRatio(horisontalWidth, verticalHeight);
}

Camera::Camera(std::string name, int width, int height, float fieldOfViewAngle): SceneObject(name)
{
        this->SetFOV(fieldOfViewAngle);
        this->SetAspectRatio(width, height);
}

void Camera::SetFOV(float FieldOfViewHorizontalAngle){

        float horisontalWidth = this->horizontalOffset.x();
        float verticalHeight = this->verticalOffset.y();

        float fieldOfViewAngleInRadians = FieldOfViewHorizontalAngle * (pi / 180);
        float focalLengthCalculated = horisontalWidth / ( 2 * tan(fieldOfViewAngleInRadians / 2));

        this->frustumLength = focalLengthCalculated;
        this->lower_left_corner = this->origin + vec3(-1 * horisontalWidth / 2, -1 * verticalHeight /2, -1 * focalLengthCalculated);
        this->fovDegrees = FieldOfViewHorizontalAngle;
}
void Camera::SetAspectRatio(float widthInPixels, float heightInPixels)
{              
        float horisontalWidth = this->horizontalOffset.x();
        float verticalHeight = this->verticalOffset.y();

        float pixelToWorldRatio = widthInPixels / horisontalWidth;
        verticalHeight = heightInPixels / pixelToWorldRatio;

        this->lower_left_corner = this->origin + vec3(-1 * horisontalWidth / 2, -1 * verticalHeight /2, -1 * this->frustumLength);
        this->verticalOffset = vec3(this->verticalOffset.x(), verticalHeight, this->verticalOffset.z());
        this->internalWidth = widthInPixels;
        this->internalHeight = heightInPixels;
}
void Camera::SetPosition(vec3 position){
        this->origin = position;
        this->SetFOV(this->fovDegrees);
        this->SetAspectRatio(this->internalWidth, this->internalHeight);
}
void Camera::SetRotation(vec3 rotation){
        this->rotation = rotation;
};


vec3 Camera::GetFocalPoint() const{
        return this->focalPoint;
}
float Camera::GetApertureSize() const{
        return this->apertureSize;
}
void Camera::SetFocalPoint(vec3 focalPointToSet){
        this->focalPoint = focalPointToSet;
}
void Camera::SetApertureSize(float apertureSizeToSet){
        this->apertureSize = apertureSizeToSet;
}
bool Camera::GetDofStatus() const{
        return this->dofEnabled;
};
void Camera::SetDofStatus(bool dofStatus){
        this->dofEnabled = dofStatus;
}


ray Camera::get_ray(float u, float v) const
{
        if(this->dofEnabled) {
                vec3 positionOnImagePlane = lower_left_corner + u * horizontalOffset + v * verticalOffset - this->origin;
                double distanceOnRay = 0;
                
                vec3 rayDirection = RtMath::RotateByAxis(positionOnImagePlane, this->rotation);
                ray originalRay = ray(origin, rayDirection);

                if(RtMath::PlaneRayIntersection(this->focalPoint, this->focalPoint - this->origin, originalRay, distanceOnRay)) 
                {
                        vec3 shiftedCameraPosition;
                        if(this->bladesEnabled){
                                shiftedCameraPosition = RtMath::RandomPointOnANgon(this->apertureSize, this->bladesNumber, this->bladesRotation) + this->origin;
                        } else {
                                shiftedCameraPosition = RtMath::RandomPointOnADiskXY(this->apertureSize) + this->origin;
                        }

                        vec3 newDirection = originalRay.point_at_parameter(distanceOnRay) - shiftedCameraPosition;
                        return ray(shiftedCameraPosition, newDirection);
                } else {
                        return ray(origin, lower_left_corner + u * horizontalOffset + v * verticalOffset - origin);
                }
        } else {
                vec3 rayDirection = lower_left_corner + u * horizontalOffset + v * verticalOffset - origin;
                rayDirection = RtMath::RotateByAxis(rayDirection, this->rotation);
                return ray(origin, rayDirection);
        }
}

bool Camera::GetBladesStatus(){
        return this->bladesEnabled;
}
int Camera::GetBladesNumber(){
        return this->bladesNumber;
}
float Camera::GetBladesRotation(){
        return this->bladesRotation;
}
void Camera::SetBladesStatus(bool bladesEnabled){
        this->bladesEnabled = bladesEnabled;
}
void Camera::SetBlades(int bladesNumber, float bladesRotation){
        this->bladesNumber = bladesNumber;
        this->bladesRotation = bladesRotation;
}
