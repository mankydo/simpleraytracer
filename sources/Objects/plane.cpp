#include "plane.h"
#include "RtMath.h"


plane::plane(std::string name, IMaterial * material): SceneObject(name)
{
    this->normal = unit_vector(vec3(0,1,0));
    this->_MaterialPointer = material;
}

vec3 plane::getOrigin(){
    return this->origin;
}
vec3 plane::getNormal(){
    return this->normal;
}

IMaterial * plane::GetMaterial() const{
    return _MaterialPointer;
}

bool plane::Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const{
    bool result = false;

    double hit_result = 0;
//    vec3 objOrigin = this->Position();
    vec3 objOrigin = vec3(0);

    result = RtMath::PlaneRayIntersection(objOrigin, this->normal, in_ray, hit_result);

//    hit_result -= 3.5;

    if(result) {

        output_info.materialPointer = this->GetMaterial();       
       
        if(hit_result < t_max && hit_result > t_min){
            output_info.t = hit_result;
            output_info.normal = unit_vector(this->normal);
            output_info.setFaceNormal(in_ray, output_info.normal);    
            result = true;      
        } else {
            result = false;
        }
    } else {
        result = false;
    } 
    return result;
}

