#ifndef NGONTESTRENDERER_H
#define NGONTESTRENDERER_H

#include "image.h"
#include "vec2.h"

class NgonTestRenderer
{
public:
    ~NgonTestRenderer(){};
    void render(image &image, int inSamples);

    void DrawLine(image & inImage, vec2 pointA, vec2 pointB, color4 lineColor);
    void DrawNgon(image & inImage, vec2 position, float radius, int numberOfLeafs, color4 ngonColor, int samples);
    void DrawNgonShared(image & inImage, vec2 position, float radius, int numberOfLeafs, float rotateAngleInRadians, color4 ngonColor, int samples);

    float GetYOfLine(float const & x, vec2 const & pointA, vec2 const & pointB);

};

#endif