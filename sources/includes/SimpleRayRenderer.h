#ifndef SIMPLERAYRENDERER_H
#define SIMPLERAYRENDERER_H

#include "image.h"
#include "Ray.h"
#include "IRenderer.h"
#include "color4.h"
#include "HittableObject.h"
#include "Camera.h"


class SimpleRayRenderer : public IRenderer
{
private:
    std::vector<HittableObject *> GetHittableObjectsToRender(const Scene &in_scene, bool printDebug);
    color4 RayColor(const ray &r, std::vector<HittableObject *> &objectsToRender, IBackground *background, int bouncesMax = 10, int bounce = 0);
public:
    ~SimpleRayRenderer(){};
    void render(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters) override;
    void renderBackground(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters);
};

#endif