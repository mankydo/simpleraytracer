#ifndef ENUMS_H
#define ENUMS_H


enum ObjectTypes {hittableObjects, lightsObjects, camerasObjects, helpersObjects, Materials, Scenes};
enum ObjectEventsEnum {ObjectCreated, ObjectDeleted};

#endif