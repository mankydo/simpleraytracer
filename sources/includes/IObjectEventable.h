#ifndef IOBJECTEVENTABLE_H
#define IOBJECTEVENTABLE_H

#include "Object.h"
#include "IObjectObverver.h"
#include "enums.h"

//Interface for object events
class IObjectEventable {
    public:
    virtual ~IObjectEventable(){}
    virtual void AddListener(IObjectObverver &observerToSubscribe)=0;
    virtual void RemoveListener(IObjectObverver &observerToSubscribe)=0;
    virtual void Invoke(ObjectEventsEnum event, Object &sender)=0;
};

#endif