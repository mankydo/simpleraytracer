#ifndef SIMPLEGRADIENTBACKGROUND_H
#define SIMPLEGRADIENTBACKGROUND_H

#include "IBackground.h"

//Interface for a scene background
class SimpleGradientBackground : public IBackground
{
    private:
        vec3 _color_bottom, _color_up;

    public:
        SimpleGradientBackground(std::string name, vec3 color_bottom, vec3 color_up);
        color4 GetColorAtDirection(const ray &in_ray) override;
};

#endif