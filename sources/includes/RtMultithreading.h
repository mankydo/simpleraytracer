#ifndef RTMULTITHREADING_H
#define RTMULTITHREADING_H

#include <thread>
#include <future>
#include "IRenderer.h"
#include "image.h"
#include "RtUtilities.h"

namespace RaytraceMultithread
{
    image RenderMultithreaded(IRenderer * renderer, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters);

    image ExecuteRender(int samples, IRenderer * renderer, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters);

    struct ThreadFutures{
        std::thread m_thread;
        std::future<image> m_future;
    };
} 
#endif
