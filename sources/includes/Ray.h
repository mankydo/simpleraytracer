#ifndef RAY_H
#define RAY_H

#include "vec3.h"

class ray
{
private:
    vec3 ray_origin;
    vec3 ray_direction;

public:
    ray() {}
    ray(const vec3 & origin, const vec3 &direction){
        ray_origin = origin;
        ray_direction = unit_vector(direction);
    }
    
    vec3 origin() const {
        return ray_origin;
    }
    vec3 direction() const {
        return ray_direction;
    }
    vec3 point_at_parameter(float t) const {
        return ray_origin + t * ray_direction;
    }
};

#endif