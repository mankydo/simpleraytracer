#ifndef VEC2_H
#define VEC2_H


struct vec2
{
    float x;
    float y;

    inline vec3 toVec3() const { return vec3(x, y, 0);}
};

inline vec2 operator*(vec2 const & pointA, vec2 const &pointB)
{
    return vec2{pointA.x * pointB.x, pointA.y * pointB.y};
}

inline vec2 operator*(float const & number, vec2 const &pointA)
{
    return vec2{number * pointA.x,number * pointA.y};
}

inline vec2 operator*(vec2 const &pointA, float const & number)
{
    return vec2{number * pointA.x,number * pointA.y};
}

inline vec2 operator+(vec2 const & pointA, vec2 const &pointB)
{
    return vec2{pointA.x + pointB.x, pointA.y + pointB.y};
}

inline vec2 operator-(vec2 const & pointA, vec2 const &pointB)
{
    return vec2{pointA.x - pointB.x, pointA.y - pointB.y};
}


#endif