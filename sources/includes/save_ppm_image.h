#ifndef SAVE_PPM_IMAGE_H
#define SAVE_PPM_IMAGE_H

#include "image.h"
#include "ISaveImage.h"

class SaveAsPPMImage : public ISaveImage {
    public:
        ~SaveAsPPMImage() {}
        int saveAsImage(const image &in_image, std::string image_name = "", bool gammaCorrected = true);
};

#endif