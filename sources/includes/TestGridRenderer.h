#ifndef TESTGRIDRENDERER_H
#define TESTGRIDRENDERER_H

#include "image.h"
#include "IRenderer.h"


class TestGridRenderer : public IRenderer
{
public:
    ~TestGridRenderer(){};
    void render(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters) override;
};

#endif