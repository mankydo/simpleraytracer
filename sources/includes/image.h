#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include <cmath>
#include "vec3.h"
#include "color4.h"

struct imageRenderStats
{
    double rendertime = -1;
    double renderstart = -1;
    double renderend = -1;
};

enum imageMathOperationsEnum{
    Add, Substract, Divide, Multiply
};

class image
{
private:
    int width;
    int height;
    float gamma = 2.0;
    int samples = 1;
    imageRenderStats renderStats = {};

    // actually it is make more sense to use vector here as well 
    // but since I've gone an array route let's go with it till the end
//    vec3 *pixels;
    std::vector<color4> pixels;

    int clampInt(int x, int clamp_low, int clamp_high) const;
    void MathOnImage(color4 color, imageMathOperationsEnum operation);

public:
    image(int width, int height, color4 color = color4(0,0,0,1), bool array = true);
    ~image();

    //get a pixel at x,y
    color4 getPixelRaw(int x, int y) const;
    color4 getPixelGammaCorrected(int x, int y) const;
    void setpixel(int x, int y, color4 color);
    int GetSamples();
    void setSamples(int samples);
    image Crop(int newWidth, int newHeight);

    imageRenderStats getrenderstats();
    double getrendertime();
    double getrenderstart();
    double getrenderend();
    void setrenderstats(double in_rendertime, double in_renderstart = -1, double in_renderend = -1);
    void setrenderstats(imageRenderStats in_renderstats);
    int getwidth() const;
    int getheight() const;
    void clear();
    void fillWithColor(color4 color);

    //implemented in imageMath.cpp
    image operator+(const vec3 & colorWithNoAlpha);
    image operator+(const float & number);
    image operator+(const color4 & color);
    image operator+(const image & image2);

    image operator-(const vec3 & colorWithNoAlpha);
    image operator-(const float & number);
    image operator-(const color4 & color);
    image operator-(const image & image2);

    image operator*(const vec3 & colorWithNoAlpha);
    image operator*(const float & number);
    image operator*(const color4 & color);
    image operator*(const image & image2);

    image operator/(const vec3 & colorWithNoAlpha);
    image operator/(const float & number);
    image operator/(const color4 & color);
    image operator/(const image & image2);

    image ImageMath(const image & image1, const image & image2, imageMathOperationsEnum operation);
    image ImageMath(const image & image1, const vec3 colorWithNoAlpha2, imageMathOperationsEnum operation);
    image ImageMath(const image & image1, const float number2, imageMathOperationsEnum operation);
    image ImageMath(const image & image1, const color4 color2, imageMathOperationsEnum operation);

    void AddWithAlpha(const image & imageToAddWithAlpha, bool alphaPremultiplied = true);

    void NormalizeBySamples(int samples);
    void NormalizeBySamples();
};


#endif