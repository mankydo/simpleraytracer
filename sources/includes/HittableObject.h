#ifndef HITTABLEOBJECT_H
#define HITTABLEOBJECT_H

#include "IHittable.h"
#include "SceneObject.h"

class HittableObject: public IHittable, public SceneObject
{
public:
    virtual bool Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const = 0;
    virtual ~HittableObject(){};
};

#endif