#ifndef INPUTSETTINGS_H
#define INPUTSETTINGS_H

#include <string>
#include <iostream>

    class InputSettings
    {
    private:

    void ParseDimensions(std::string inputArg);
    void ParseSamples(std::string inputArg);
    void ParseBounces(std::string inputArg);

    public:

        bool in_invert = false;
        bool in_add_grid = false;
        //for future use: save either as ppm or bmp; in future make it enum
        bool in_saveAsBmp = false;
        bool in_userays = true;
        int dimensionX = 200;
        int dimensionY = 100;
        int numberOfSamples = 100;
        int numberOfBounces = 130;
//        std::string settingsFilename = "settings.txt";
        std::string settingsFilename = "";
        bool debugLogOutput = false;
        bool output2d = false;

        bool isChangedDimensions = false;
        bool isChangedSamples  = false;
        bool isChangedBounces = false;

        void ParseAndUpdateSettings(int argc, char** argv);
    };
        

#endif