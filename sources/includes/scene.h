#ifndef SCENE_H
#define SCENE_H

#include <map>
#include <vector>
#include "IHittable.h"
#include "SceneObject.h"
#include "IBackground.h"

struct SceneObject_properties
{
    unsigned int object_id;
    std::string object_name;
    ObjectTypes object_type;
    Object *object_pointer;
};


class Scene: IObjectObverver, public Object
{
private:
//  scene info struct of some kind, background, ambient color, scale maybe?
//  a vector(list) containing pointers to renderable objects(IHittable) in a scene
//  a vector containig lights(each light is a separate class, obviously) in a scene

    unsigned int _current_id = 0;
    std::string _scene_name = "";
    IBackground *_sceneBackground; 
    std::map<unsigned int, SceneObject_properties> _objectList;
    std::map<unsigned int, SceneObject_properties> _cachedHittableObjectList;
    unsigned int SetNewId();
    unsigned int GetCurrentId() const;

public:
//  a hit method to check if we hit something via raytrace 
//      and return a point and a normal if it is so
// getobject method
// addobject method
    Scene(std::string sceneName, IBackground *sceneBackground);
    ~Scene();
    void GetObjectMessage(ObjectEventsEnum event, const Object &sender) override;
    std::string Name() const;
    Object *GetObject(std::string sceneObject_name) const;
    Object *GetObjectBySceneObjectId(unsigned int sceneObject_id) const;
    std::vector<Object *> GetObjectsTypeOf(ObjectTypes typeToReturnListOf) const;
    std::vector<Object *> GetSceneObjects() const;
    void addObject(SceneObject *object_to_add);
    std::string addObject(SceneObject *object_to_add, std::string desiredObjectNameInAScene);
    void RemoveObjectFromScene(unsigned int object_id);
    IBackground *GetBackground() const;

};

#endif