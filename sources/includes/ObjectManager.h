#ifndef OBJECTMANAGER_H
#define OBJECTMANAGER_H

#include <iostream>
#include <map>
#include <string>
#include "Object.h"

class ObjectManager: IObjectObverver
{
    private:
        // change pointer to unique_pointer later
        std::map<unsigned int, Object*> _objectList;
        static ObjectManager *instance;
        unsigned int _current_id = 0;
        ObjectManager();
        unsigned int SetNewId();
        unsigned int GetCurrentId() const;

    public:
        static ObjectManager *Instance();
        void Init(){};
        void GetObjectMessage(ObjectEventsEnum event, const Object &sender) override;
        Object *GetObject(unsigned int object_id) const;
        Object *GetObject(std::string object_name) const;
        //returns id if successful, 0 if failed
        unsigned int AddObjectToManager(Object &objectToAdd);
        int RemoveObjectFromManager(unsigned int object_id);
        //iterate through map and delete all objects in it
        void DeleteAllObjects();
        ~ObjectManager();
};

#endif