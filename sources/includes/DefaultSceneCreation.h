#ifndef DEFAULTSCENECREATIION_H
#define DEFAULTSCENECREATIION_H

#include "IBackground.h"
#include "SimpleGradientBackground.h"
#include "color4.h"
#include "vec3.h"
#include "scene.h"
#include "Materials.h"
#include "sphere.h"
#include "IRenderer.h"

Scene * DefaultSceneCreation(const RenderParameters &renderParameters){
        //create a background object
    IBackground *simpleBackground = new SimpleGradientBackground(
            "background",
            renderParameters.color_bottom, 
            renderParameters.color_up
        );
//    Scene scene = Scene("scene one", simpleBackground);
    Scene * scene = new Scene("scene one", simpleBackground);

    IMaterial * lambert = new MaterialLambert("cyan lambert", color4(.2, .7, .5, 1));
    IMaterial * lambertRed = new MaterialLambert("red lamberttt", color4(.8, .2, .2, 1));
    IMaterial * metallicRed = new MaterialMetallic("metallic red", color4(.8, .1, .2, 1), .4);
    IMaterial * metallicBlue = new MaterialMetallic("metallic blue", color4(.5, .5, .8, 1), .98);

//    std::cout << "Created material called: " << lambert->Name << "\n";
//    std::cout << "Created material called: " << lambertRed->Name << "\n";


    SceneObject * newObject;
    newObject = new sphere(100, "sphere 2", lambert);
    newObject->SetPosition(vec3(0, -100.5, -1));
    scene->addObject(newObject);

    newObject = new sphere(0.5, "sphere 1", lambertRed);
    newObject->SetPosition(vec3(.2, .1, -1.0));
    scene->addObject(newObject);

    newObject = new sphere(0.2, "sphere 1.4", lambert);
    newObject->SetPosition(vec3(.2, .1, -.6));
    scene->addObject(newObject);

    newObject = new sphere(0.5, "sphere 3", lambertRed);
    newObject->SetPosition(vec3(-0.5, -0.5, -1.4));
    scene->addObject(newObject);

    newObject = new sphere(0.15, "sphere metallic", metallicRed);
    newObject->SetPosition(vec3(-.28, .2, -.6));
    scene->addObject(newObject);

    newObject = new sphere(0.2, "sphere metallic", metallicBlue);
    newObject->SetPosition(vec3(-.4, -.1, -.6));
    scene->addObject(newObject);

    return scene;
}

#endif