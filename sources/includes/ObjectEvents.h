#ifndef OBJECTEVENTS_H
#define OBJECTEVENTS_H

#include <vector>
#include "Object.h"
#include "IObjectObverver.h"
#include "IObjectEventable.h"

//Interface for object events
class ObjectEvents: IObjectEventable
 {
    private:
        std::vector<IObjectObverver *> _listOfSubscribers; 
    public:
        void AddListener(IObjectObverver &observerToSubscribe) override;
        void RemoveListener(IObjectObverver &observerToUnsubscribe) override;
        void Invoke(ObjectEventsEnum event, Object &sender) override;
};

#endif