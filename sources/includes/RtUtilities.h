#ifndef RTUTILITIES_H
#define RTUTILITIES_H

#include <iostream>
#include <string>
#include <vector>
#include "vec3.h"
#include "color4.h"

namespace RtUtilities{

    enum LogColor {
        Red     = 31, 
        Grey    = 90, 
        Orange  = 33, 
        Cyan    = 36, 
        Green   = 32, 
        White   = 37, 
        Blue    = 34, 
        Magenta = 35,
        Default = 39 
    };

/*     enum ColorCode {
        FG_RED      = 31,
        FG_GREEN    = 32,
        FG_BLUE     = 34,
        FG_DEFAULT  = 39,
        BG_RED      = 41,
        BG_GREEN    = 42,
        BG_BLUE     = 44,
        BG_DEFAULT  = 49
    }; */

    void PrintProgress(float progress01);
    void Log(const std::string & message, int indent, LogColor color);

    void Log(const std::string & message);
    void Log(const std::string & message, int indent);
    void Log(const std::string & message, LogColor color);

    void DebugLog(const std::string & message);
    void DebugLog(const std::string & message, int indent);
    void DebugLog(const std::string & message, LogColor color);
    void DebugLog(const std::string & message, int indent, LogColor color);


    void StringToLowercase(std::string & stringToConvert);
    void TrimWhitespaces(std::string & stringToConvert);

    int ConvertPropertyToInt(const std::string & property, bool &convertSuccessful);
    float ConvertPropertyToFloat(const std::string & property, bool &convertSuccessful);
    vec3 ConvertPropertyToVec3(const std::string & property, bool &convertSuccessful);
    color4 ConvertPropertyToColor4(const std::string & property, bool &convertSuccessful);
    std::vector<float> ConvertMultivecPropertyToVecSized(const std::string & property, const int vecSize, bool &convertSuccessful);
}

#endif
