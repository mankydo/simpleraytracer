#ifndef RTMATH_H
#define RTMATH_H

#include <functional>
#include <random>
#include "vec3.h"
#include "vec2.h"
#include "matrix3x3.h"
#include "Ray.h"

namespace RtMath{

    double random_double();
    double random_double(double min, double max);

    inline vec3 RandomVec3(){
        return vec3(RtMath::random_double(), RtMath::random_double(), RtMath::random_double());
    }

    inline vec3 RandomVec3(double min, double max){
        return vec3(RtMath::random_double(min, max), RtMath::random_double(min, max), RtMath::random_double(min, max));
    } 
 
    vec3 RandomInUnitSphere();
    vec3 RandomUnitVector();
    vec3 RandomPointOnADiskXY();
    vec3 RandomPointOnADiskXY(float radius);
    vec3 RandomPointOnADiskXY(float radius, vec3 origin);

    vec3 RandomPointOnANgon(int numberOfLeafs);
    vec3 RandomPointOnANgon(float radius, int numberOfLeafs, float rotateAngleInRadians);

    bool PlaneRayIntersection(const vec3 planeOrigin, const vec3 planeNormal, const ray & rayToIntersect, vec3 & pointOfIntersection);
    bool PlaneRayIntersection(const vec3 planeOrigin, const vec3 planeNormal, const ray & rayToIntersect, double & distanceOnRayResult);
    bool XYQuadRayIntersection(const ray & rayToIntersect, 
        const float & xMin, 
        const float & xMax, 
        const float & yMin, 
        const float & yMax, 
        double & distanceOnRayResult);

    bool YZQuadRayIntersection(const ray & rayToIntersect, 
        const float & yMin, 
        const float & yMax, 
        const float & zMin, 
        const float & zMax, 
        double & distanceOnRayResult);

    bool XZQuadRayIntersection(const ray & rayToIntersect, 
        const float & xMin, 
        const float & xMax, 
        const float & zMin, 
        const float & zMax, 
        double & distanceOnRayResult);



    float lerp(float const & pointA, float const & pointB, float factor01);
    vec2 lerp(vec2 const & pointA, vec2 const & pointB, float factor01);
    vec3 lerp(vec3 const & pointA, vec3 const & pointB, float factor01);

    vec3 RotateByAxis(vec3 const & vec3ToRotate, vec3 axisToRotate);
    vec3 RotateByAxisInverse(vec3 const & vec3ToRotate, vec3 axisToRotate);
    vec3 UnRotateByAxis(vec3 const & vec3ToRotate, vec3 axisToRotate);

    namespace Matrices{
        Matrix3x3 RotationMatrix(const vec3 & axisToRotateInRadians);
        Matrix3x3 Inverse(const Matrix3x3 & matrixToInverse);
        Matrix3x3 Transpose(const Matrix3x3 & matrixToTranspose);
        float DeterminantForElement(const Matrix3x3 & matrixToCalculate, int column, int row);
    }

}

#endif