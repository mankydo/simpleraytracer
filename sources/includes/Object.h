#ifndef OBJECT_H
#define OBJECT_H

#include <string>
#include "ObjectEvents.h"
#include "enums.h"
#include "vec3.h"
#include "RtMath.h"
#include "matrix3x3.h"

class ObjectEvents;

class Object
{
private:
    unsigned int object_id;
// type of the object: light/geometry/helpers
    ObjectTypes object_type;
    std::string objectName;

protected: 
    int SetObjectType(ObjectTypes newObjectType);

public:
    static ObjectEvents events;
    Object(std::string name);
    Object(std::string name, ObjectTypes type);
    virtual ~Object();
    ObjectTypes GetObjectType() const;
    unsigned int GetObjectId() const;
    std::string GetName() const;
    int SetName(std::string newName);
};

#endif