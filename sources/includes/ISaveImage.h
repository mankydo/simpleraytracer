#ifndef ISAVEIMAGE_H
#define ISAVEIMAGE_H

#include "image.h"

//Interface for saving images
class ISaveImage {
    public:
    virtual ~ISaveImage(){}
    virtual int saveAsImage(const image &in_image, std::string image_name = "", bool gammaCorrected = true) = 0;
};

#endif