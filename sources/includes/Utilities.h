#ifndef UTILITIES_H
#define UTILITIES_H

#include <cmath>
#include <cstdlib>
#include <limits>
#include <memory>

using std::shared_ptr;
using std::make_shared;
using std::sqrt;

const double infinity = std::numeric_limits<double>::infinity();
const double pi = 3.14159265897932385;

inline double degreesToRadians(double degrees) {
    return degrees * pi / 180;
}

inline float clamp01(float a){
    return (float)std::min(1.0, (std::max(0.0, (double)a) ));
}

#endif