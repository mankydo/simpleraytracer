#ifndef IOBJECTOBSERVER_H
#define IOBJECTOBSERVER_H

#include "Object.h"
#include "enums.h"

class Object;

//Interface for object events
class IObjectObverver {
    public:
//    virtual ~IObjectObverver(){}
    virtual void GetObjectMessage(ObjectEventsEnum event, const Object &sender)=0;
};

#endif