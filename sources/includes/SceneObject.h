#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <string>
#include "Object.h"
#include "vec3.h"
#include "RtMath.h"
#include "matrix3x3.h"

class ObjectEvents;

class SceneObject: public Object
{
protected: 
// type of the object: light/geometry/helpers
    vec3 position;
    vec3 rotation;
    vec3 scale;
    Matrix3x3 rotationMatrix = Matrix3x3();
    Matrix3x3 rotationMatrixInverse = Matrix3x3();

public:
    SceneObject(std::string name);
    SceneObject(std::string name, ObjectTypes type);

    void SetTransform(vec3 position);
    void SetTransform(vec3 position, vec3 rotation);
    void SetTransform(vec3 position, vec3 rotation, vec3 scale);

    vec3 Position() const;
    vec3 Rotation() const;
    Matrix3x3 RotationMatrix() const;
    Matrix3x3 RotationMatrixInverse() const;
    vec3 Scale() const;

    virtual void SetPosition(vec3 position);
    virtual void SetRotation(vec3 rotation);
    virtual void SetScale(vec3 scale) {};
};

#endif