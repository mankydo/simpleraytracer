#ifndef IHITTABLE_H
#define IHITTABLE_H

#include "Ray.h"
class IMaterial;

struct hit_record
{
    float t;
    vec3 p;
    vec3 normal;
    bool frontFace;
    IMaterial * materialPointer = nullptr;

    inline void setFaceNormal(const ray& r, const vec3& outwardNormal){
        frontFace = dot(r.direction(), outwardNormal) < 0;
        normal = frontFace ? outwardNormal : -outwardNormal;
    }
};


//Interface for hitting points by ray
class IHittable {
    public:
    virtual bool Hit(const ray &in_ray, float t_min, float t_max, hit_record &output_info) const = 0;
    virtual ~IHittable(){};
};

#endif