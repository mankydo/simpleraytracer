#ifndef MATRIX3X3_H
#define MATRIX3X3_H

#include "vec2.h"

void MatricesTest();

class Matrix3x3{
    public:
    Matrix3x3() {
        e[0][0] = 1; 
        e[0][1] = 0;
        e[0][2] = 0;

        e[1][0] = 0; 
        e[1][1] = 1;
        e[1][2] = 0;

        e[2][0] = 0; 
        e[2][1] = 0;
        e[2][2] = 1;
    }
    Matrix3x3(
        float row0_0, float row0_1, float row0_2,
        float row1_0, float row1_1, float row1_2,
        float row2_0, float row2_1, float row2_2
    ) {
        e[0][0] = row0_0; 
        e[0][1] = row0_1;
        e[0][2] = row0_2;

        e[1][0] = row1_0; 
        e[1][1] = row1_1;
        e[1][2] = row1_2;

        e[2][0] = row2_0; 
        e[2][1] = row2_1;
        e[2][2] = row2_2;
    }

    Matrix3x3(float e0) {
        e[0][0] = e0; 
        e[0][1] = e0;
        e[0][2] = e0;

        e[1][0] = e0; 
        e[1][1] = e0;
        e[1][2] = e0;

        e[2][0] = e0; 
        e[2][1] = e0;
        e[2][2] = e0;
    }

    inline const Matrix3x3& operator+() const {return *this;}
//    inline Matrix3x3 operator-() const {return Matrix3x3(-e[0], -e[1], -e[2]);}

    inline Matrix3x3& operator+=(const Matrix3x3 &v2);
    inline Matrix3x3& operator-=(const Matrix3x3 &v2);
    inline Matrix3x3& operator*=(const Matrix3x3 &v2);
    inline Matrix3x3& operator/=(const Matrix3x3 &v2);
    inline Matrix3x3& operator*=(const float t);
    inline Matrix3x3& operator/=(const float t);

    float e[3][3];

};

inline Matrix3x3 operator*(float const &const1, Matrix3x3 const & matrix)
{
    Matrix3x3 result = Matrix3x3();
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            result.e[i][j] = matrix.e[i][j] * const1;
        }
    }
    return result;
}

inline vec2 operator*(vec2 const &pointA, Matrix3x3 const & matrix)
{
    float row0 = pointA.x * matrix.e[0][0] + pointA.y * matrix.e[1][0] + matrix.e[2][0];
    float row1 = pointA.x * matrix.e[0][1] + pointA.y * matrix.e[1][1] + matrix.e[2][1];
    return vec2{row0,row1};
}

inline vec3 operator*(vec3 const &pointA, Matrix3x3 const & matrix)
{
    float row0 = pointA.e[0] * matrix.e[0][0] + pointA.e[1] * matrix.e[1][0] + pointA.e[2] * matrix.e[2][0];
    float row1 = pointA.e[0] * matrix.e[0][1] + pointA.e[1] * matrix.e[1][1] + pointA.e[2] * matrix.e[2][1];
    float row2 = pointA.e[0] * matrix.e[0][2] + pointA.e[1] * matrix.e[1][2] + pointA.e[2] * matrix.e[2][2];
    return vec3(row0,row1,row2);
}


inline Matrix3x3 operator*(Matrix3x3 const & matrix1, Matrix3x3 const & matrix2)
{
    Matrix3x3 result = Matrix3x3();

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            result.e[i][j] = matrix1.e[i][0] * matrix2.e[0][j] + matrix1.e[i][1] * matrix2.e[1][j] + matrix1.e[i][2] * matrix2.e[2][j];
        }
    }

    return result;
}

inline Matrix3x3& Matrix3x3::operator*=(const Matrix3x3 &v2){
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            e[i][j] = e[i][0] * v2.e[0][j] + e[i][1] * v2.e[1][j] + e[i][2] * v2.e[2][j];
        }
    }
    return *this;
}



#endif