#ifndef color4_H
#define color4_H

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include "vec3.h"

class color4{
    public:
    color4() {}
    color4(float e0, float e1, float e2, float e3) {
        e[0] = e0; 
        e[1] = e1;
        e[2] = e2;
        e[3] = e3;
    }

    color4(vec3 vec, float e3) {
        e[0] = vec.r(); 
        e[1] = vec.g();
        e[2] = vec.b();
        e[3] = e3;
    }

    color4(float e0) {
        e[0] = e0; 
        e[1] = e0;
        e[2] = e0;
        e[3] = e0;
    }

    inline float x() const {return e[0];}
    inline float y() const {return e[1];}
    inline float z() const {return e[2];}
    inline float w() const {return e[3];}
    inline float r() const {return e[0];}
    inline float g() const {return e[1];}
    inline float b() const {return e[2];}
    inline float a() const {return e[3];}
    inline void a(float a) {
        this->e[3] = a;
    }
    
    inline const color4& operator+() const {return *this;}
    inline color4 operator-() const {return color4(-e[0], -e[1], -e[2], -e[3]);}
    inline float operator[](int i) const {return e[i];}
    inline float& operator[](int i) {return e[i];}

    inline color4& operator+=(const color4 &v2);
    inline color4& operator-=(const color4 &v2);
    inline color4& operator*=(const color4 &v2);
    inline color4& operator/=(const color4 &v2);
    inline color4& operator*=(const float t);
    inline color4& operator/=(const float t);

    inline vec3 toVec3() const { return vec3(e[0], e[1], e[2]);}

    static inline float clamp01(float a);

    float e[4];

};

inline float color4::clamp01(float a){
    return (float)std::min(1.0, (std::max(0.0, (double)a) ));
}

inline std::istream& operator>>(std::istream &is, color4 &t){
    is >> t.e[0] >> t.e[1] >> t.e[2] >> t.e[3];
    return is;
}

inline std::ostream& operator<<(std::ostream &os, const color4 &t){
    os << t.e[0] << " " << t.e[1] << " " << t.e[2] << t.e[3];
    return os;
}

inline color4 operator+(const color4 &v1, const color4 &v2){
    return color4(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2], v1.e[3] + v2.e[3]);
}

inline color4 operator-(const color4 &v1, const color4 &v2){
    return color4(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2], v1.e[3] - v2.e[3]);
}

inline color4 operator*(const color4 &v1, const color4 &v2){
    return color4(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2], v1.e[3] * v2.e[3]);
}

inline color4 operator*(const color4 &v, float t){
    return color4(v.e[0] * t, v.e[1] * t, v.e[2] * t, v.e[3] * t);
}

inline color4 operator*(float t, const color4 &v){
    return color4(v.e[0] * t, v.e[1] * t, v.e[2] * t, v.e[3] * t);
}

inline color4 operator/(const color4 &v1, const color4 &v2){
    return color4(v1.e[0] / v2.e[0], v1.e[1] / v2.e[1], v1.e[2] / v2.e[2], v1.e[3] / v2.e[3]);
}

inline color4 operator/(const color4 &v, float t){
    return color4(v.e[0] / t, v.e[1] / t, v.e[2] / t, v.e[3] / t);
}

inline color4& color4::operator+=(const color4 &v){
    e[0] += v.e[0];
    e[1] += v.e[1];
    e[2] += v.e[2];
    e[3] += v.e[3];
    return *this;
}

inline color4& color4::operator-=(const color4 &v){
    e[0] -= v.e[0];
    e[1] -= v.e[1];
    e[2] -= v.e[2];
    e[3] -= v.e[3];
    return *this;
}

inline color4& color4::operator*=(const color4 &v){
    e[0] *= v.e[0];
    e[1] *= v.e[1];
    e[2] *= v.e[2];
    e[3] *= v.e[3];
    return *this;
}

inline color4& color4::operator*=(const float t){
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    e[3] *= t;
    return *this;
}

inline color4& color4::operator/=(const color4 &v){
    e[0] /= v.e[0];
    e[1] /= v.e[1];
    e[2] /= v.e[2];
    e[3] /= v.e[3];
    return *this;
}

inline color4& color4::operator/=(const float t){
    e[0] /= t;
    e[1] /= t;
    e[2] /= t;
    e[3] /= t;
    return *this;
}

#endif