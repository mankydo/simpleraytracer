#ifndef IRENDERER_H
#define IRENDERER_H

#include <string>
#include "image.h"
#include "scene.h"
#include "vec3.h"
#include "Camera.h"
#include "Utilities.h"

struct RenderParameters
{
    int width = 200;
    int height = 100;
    bool inverty = true;
    bool add_grid = false;
    int samples = 5;
    int raysNumber = 10;
    int bounces = 5;
    vec3 color_up = vec3(.5, .7, 1.0);
    vec3 color_bottom = vec3(1.0, 1.0, 1.0);
    bool normalizeResult = true;
    bool printProgress = true;
    bool TransparentBackground = false;
    Scene * sceneToRender;
    Camera * cameraToRender;
};


class IRenderer
{
private:
    RenderParameters renderParameters = {};
public:
    virtual ~IRenderer(){};
    virtual void render(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters) = 0;
};

#endif