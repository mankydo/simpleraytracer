#ifndef IBACKGROUND_H
#define IBACKGROUND_H

#include "Ray.h"
#include "color4.h"
#include "Object.h"

//Interface for a scene background
class IBackground: public Object
 {
    public:
    virtual ~IBackground(){}
    IBackground(std::string name):Object(name){}
    virtual color4 GetColorAtDirection(const ray &in_ray) = 0;
    
};

#endif