#include "SimpleGradientBackground.h"

SimpleGradientBackground::SimpleGradientBackground(std::string name, vec3 color_bottom, vec3 color_up):IBackground(name)
{
    this->_color_bottom = color_bottom;
    this->_color_up = color_up;
}
color4 SimpleGradientBackground::GetColorAtDirection(const ray &in_ray){
    vec3 unit_direction = unit_vector(in_ray.direction());
    float t = .5 * (unit_direction.y() + 1.0);
    return color4((1.0 - t) * this->_color_bottom + t * this->_color_up, 1);
}