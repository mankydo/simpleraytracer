#include <iostream>
#include <fstream>
#include "save_ppm_image.h"
#include "vec3.h"
#include "image.h"
#include "ISaveImage.h"

int SaveAsPPMImage::saveAsImage(const image &in_image, std::string image_name, bool gammaCorrected){

    std::string filename = image_name == "" ? "ppm_test_image" : image_name;
    std::ofstream ppm_image(filename + ".ppm");

    int nx = in_image.getwidth();
    int ny = in_image.getheight();
 
//    std::cout << "P3\n" << nx << " " << ny << "\n255\n";
    ppm_image << "P3\n" << nx << " " << ny << "\n255\n";

    for(int j = 0; j < ny; j++){
        for(int i =0; i < nx; i++){
            color4 col;
            if(gammaCorrected){
                col =  in_image.getPixelGammaCorrected(i, j);
            } else {
                col =  in_image.getPixelRaw(i, j);
            }

            int ir = int(255.99 * col[0]);
            int ig = int(255.99 * col[1]);
            int ib = int(255.99 * col[2]);
//            std::cout << ir << " " << ig << " " << ib << "\n";
            ppm_image << ir << " " << ig << " " << ib << "\n";
        }
    }

    ppm_image.close();
    
    return 1;

}