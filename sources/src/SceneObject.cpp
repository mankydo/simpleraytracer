#include <iostream>
#include "SceneObject.h"
#include "RtUtilities.h"

SceneObject::SceneObject(std::string name) : Object(name)
{
    this->SetName(name);
    this->SetObjectType(ObjectTypes::hittableObjects);
}

void SceneObject::SetTransform(vec3 positionToSet){
    this->SetPosition(positionToSet);
}
void SceneObject::SetTransform(vec3 positionToSet, vec3 rotationToSet){
    this->SetPosition(vec3(0));
    this->SetRotation(rotationToSet);
    this->SetPosition(positionToSet);    
}
void SceneObject::SetTransform(vec3 positionToSet, vec3 rotationToSet, vec3 scaleToSet){
    this->SetPosition(vec3(0));
    this->SetRotation(rotationToSet);
    this->SetScale(scaleToSet);
    this->SetPosition(positionToSet);    
}

void SceneObject::SetPosition(vec3 in_position){
    this->position = in_position;
}
void SceneObject::SetRotation(vec3 in_rotation){
    this->rotation = in_rotation;
    this->rotationMatrix = RtMath::Matrices::RotationMatrix(this->rotation);
//    this->rotationMatrixInverse = RtMath::Matrices::Transpose(this->rotationMatrix);
    this->rotationMatrixInverse = RtMath::Matrices::Inverse(this->rotationMatrix);
}

vec3 SceneObject::Position() const{
    return this->position;
}
vec3 SceneObject::Rotation() const{
    return this->rotation;
}
Matrix3x3 SceneObject::RotationMatrix() const{
    return this->rotationMatrix;
}
Matrix3x3 SceneObject::RotationMatrixInverse() const{
    return this->rotationMatrixInverse;
}

vec3 SceneObject::Scale() const{
    return this->scale;
}