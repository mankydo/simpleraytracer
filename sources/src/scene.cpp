#include <iostream>
#include "scene.h"
#include "RtUtilities.h"

Scene::~Scene(){
    RtUtilities::DebugLog(this->_scene_name + " called for deletion\n", 1);
    Object::events.RemoveListener(*this);
}

Scene::Scene(std::string sceneName, IBackground *sceneBackground): Object(sceneName, ObjectTypes::Scenes)
{
    _scene_name = sceneName;
    _sceneBackground = sceneBackground;
    std::cout<<"scene named: "<<Name()<<" created successfuly\n";
    Object::events.AddListener(*this);
}
std::string Scene::Name() const{
    return _scene_name;
}

void Scene::GetObjectMessage(ObjectEventsEnum event, const Object &sender){
    std::string event_string = event == ObjectCreated ? "Created" : "Deleted";

    RtUtilities::DebugLog( "Scene received a message: ", 2, RtUtilities::Orange);
    RtUtilities::DebugLog( event_string, RtUtilities::Blue);
    RtUtilities::DebugLog(" on object called: ", RtUtilities::Orange);
    RtUtilities::DebugLog( sender.GetName() + "\n", RtUtilities::Default);

    if (event == ObjectDeleted){
        RemoveObjectFromScene(sender.GetObjectId());
    }
}

Object * Scene::GetObject(std::string object_name) const{
    std::map<unsigned int, SceneObject_properties> listToIterate = _objectList;
    std::map<unsigned int, SceneObject_properties>::iterator iterate = listToIterate.begin();
    while (iterate != listToIterate.end())
    {
        if(iterate->second.object_name == object_name ){
            return iterate->second.object_pointer;
        }
        iterate++;
    }
    return nullptr;
}

Object * Scene::GetObjectBySceneObjectId(unsigned int sceneObject_id) const{
    return _objectList.at(sceneObject_id).object_pointer;
}

std::vector<Object *> Scene::GetObjectsTypeOf(ObjectTypes typeToReturnListOf) const{
    std::vector<Object *> foundObjectsOfType;
    std::map<unsigned int, SceneObject_properties> listToIterate = _objectList;
    std::map<unsigned int, SceneObject_properties>::iterator iterate = listToIterate.begin();
    while (iterate != listToIterate.end())
    {
        if(iterate->second.object_type == typeToReturnListOf ){
            foundObjectsOfType.push_back(iterate->second.object_pointer);
            RtUtilities::DebugLog("Found an object in a scene: " + iterate->second.object_name + "\n", 2, RtUtilities::LogColor::Grey);
        }
        iterate++;
    }
    return foundObjectsOfType;
}

std::vector<Object *> Scene::GetSceneObjects() const{
    std::vector<Object *> foundObjects;
    std::map<unsigned int, SceneObject_properties> listToIterate = _objectList;
    std::map<unsigned int, SceneObject_properties>::iterator iterate = listToIterate.begin();
    while (iterate != listToIterate.end())
    {
        foundObjects.push_back(iterate->second.object_pointer);
        RtUtilities::DebugLog("Found an object in a scene: " + iterate->second.object_name + "\n", 2, RtUtilities::LogColor::Grey);
        iterate++;
    }
    return foundObjects;
}


unsigned int Scene::SetNewId(){
    _current_id++;
    return _current_id;
}
unsigned int Scene::GetCurrentId() const{
    return _current_id;
}


void Scene::addObject(SceneObject *object_to_add){
    // extract object properties
    // add properties into new struct inside map
    // report if adding is successful
    SceneObject_properties objectProperties= {};
    objectProperties.object_id = object_to_add->GetObjectId();
    objectProperties.object_name = object_to_add->GetName();
    objectProperties.object_type = object_to_add->GetObjectType();
    objectProperties.object_pointer = object_to_add;
    _objectList[SetNewId()] = objectProperties;

    RtUtilities::DebugLog("Object " + GetObjectBySceneObjectId(GetCurrentId())->GetName() + " was added in a scene\n", 2);
}

std::string Scene::addObject(SceneObject *object_to_add, std::string desiredObjectNameInAScene){
    std::string finalObjectNameInAScene;
    throw "not implemented yet!";
    return finalObjectNameInAScene;
}

void Scene::RemoveObjectFromScene(unsigned int object_id){
    std::map<unsigned int, SceneObject_properties> listToIterate = _objectList;
    std::map<unsigned int, SceneObject_properties>::iterator iterate = listToIterate.begin();
    while (iterate != listToIterate.end())
    {
        if(iterate->second.object_id == object_id ){
            std::string obj_name = iterate->second.object_name;
            _objectList.erase(iterate->first);
            RtUtilities::DebugLog("Removed object " + obj_name + " from a scene " + this->_scene_name + " successfully\n");
        }
        iterate++;
    }
}

IBackground * Scene::GetBackground() const{
    return _sceneBackground;
}