#include "image.h"

void image::MathOnImage(color4 color, imageMathOperationsEnum operation){
	if(operation == imageMathOperationsEnum::Add){
		for(color4 &pixel : this->pixels){
			pixel += color;
		}
	} else if(operation == imageMathOperationsEnum::Substract){
		for(color4 &pixel : this->pixels){
			pixel -= color;
		}
	} else if(operation == imageMathOperationsEnum::Multiply){
		for(color4 &pixel : this->pixels){
			pixel *= color;
		}
	} else if(operation == imageMathOperationsEnum::Divide){
		for(color4 &pixel : this->pixels){
			pixel /= color;
		}
	}
}

void image::AddWithAlpha(const image & imageToAddWithAlpha, bool alphaPremultiplied){
    int width = this->getwidth();
    int height = this->getheight();
    
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++ ){
            color4 pixelToAdd = imageToAddWithAlpha.getPixelRaw(x, y);
			float alpha = alphaPremultiplied ? pixelToAdd.a() * pixelToAdd.a() : pixelToAdd.a();
            this->setpixel(x, y, this->getPixelRaw(x, y) * (1 - alpha)  + pixelToAdd * alpha );
        }
    }
}

//overloaded image X float operations
image image::operator+(const float & number){
	return image::ImageMath(*this, number, imageMathOperationsEnum::Add);
}
image image::operator-(const float & number){
	return image::ImageMath(*this, number, imageMathOperationsEnum::Substract);
}
image image::operator*(const float & number){
	return image::ImageMath(*this, number, imageMathOperationsEnum::Multiply);
}
image image::operator/(const float & number){
	return image::ImageMath(*this, number, imageMathOperationsEnum::Divide);
}

//overloaded image X vec3 operations
image image::operator+(const vec3 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Add);
}
image image::operator-(const vec3 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Substract);
}
image image::operator*(const vec3 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Multiply);
}
image image::operator/(const vec3 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Divide);
}

//overloaded image X color4 operations
image image::operator+(const color4 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Add);
}
image image::operator-(const color4 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Substract);
}
image image::operator*(const color4 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Multiply);
}
image image::operator/(const color4 & color){
	return image::ImageMath(*this, color, imageMathOperationsEnum::Divide);
}

//overloaded image X image operations
image image::operator+(const image & image2){
	return image::ImageMath(*this, image2, imageMathOperationsEnum::Add);
}
image image::operator-(const image & image2){
	return image::ImageMath(*this, image2, imageMathOperationsEnum::Substract);
}
image image::operator*(const image & image2){
	return image::ImageMath(*this, image2, imageMathOperationsEnum::Multiply);
}
image image::operator/(const image & image2){
	return image::ImageMath(*this, image2, imageMathOperationsEnum::Divide);
}

image image::ImageMath(const image & image1,const float number2, imageMathOperationsEnum operation){
	image result = image1;
	result.MathOnImage(color4(number2,number2,number2,number2), operation);
	return result;
}
image image::ImageMath(const image & image1,const vec3 color2, imageMathOperationsEnum operation){
	image result = image1;
	result.MathOnImage(color4(color2, 1), operation);
	return result;
}
image image::ImageMath(const image & image1,const color4 color2, imageMathOperationsEnum operation){
	image result = image1;
	result.MathOnImage(color2, operation);
	return result;
}
image image::ImageMath(const image & image1,const image & image2, imageMathOperationsEnum operation){
    int newWidth = std::max(image1.getwidth(), image2.getwidth());
    int newHeight = std::max(image1.getheight(), image2.getheight());

    image result = image(newWidth, newHeight);
    if(operation == imageMathOperationsEnum::Add){
        for(int x = 0; x < newWidth; x++){
            for(int y = 0; y < newHeight; y++ ){
                result.setpixel(x, y, image1.getPixelRaw(x, y) + image2.getPixelRaw(x, y) );
            }
        }
    } else if(operation == imageMathOperationsEnum::Substract){
        for(int x = 0; x < newWidth; x++){
            for(int y = 0; y < newHeight; y++ ){
                result.setpixel(x, y, image1.getPixelRaw(x, y) - image2.getPixelRaw(x, y) );
            }
        }
    } else if(operation == imageMathOperationsEnum::Multiply){
        for(int x = 0; x < newWidth; x++){
            for(int y = 0; y < newHeight; y++ ){
                result.setpixel(x, y, image1.getPixelRaw(x, y) * image2.getPixelRaw(x, y) );
            }
        }
    } else if(operation == imageMathOperationsEnum::Divide){
        for(int x = 0; x < newWidth; x++){
            for(int y = 0; y < newHeight; y++ ){
                result.setpixel(x, y, image1.getPixelRaw(x, y) / image2.getPixelRaw(x, y) );
            }
        }
    }
    return result;
}

