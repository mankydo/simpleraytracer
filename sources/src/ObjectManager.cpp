#include <string>
#include "ObjectManager.h"
#include "RtUtilities.h"


ObjectManager * ObjectManager::instance = nullptr;

ObjectManager::ObjectManager(){
    this->_objectList.clear();
    Object::events.AddListener(*this);
//    instance = this;
}

ObjectManager *ObjectManager::Instance(){
    if(!instance){
        instance = new ObjectManager;
        return instance;
    } else {
        return instance;
    }

}

void ObjectManager::GetObjectMessage(ObjectEventsEnum event, const Object &sender){
    std::string event_string = event == ObjectCreated ? "Created" : "Deleted";

    RtUtilities::DebugLog("Received message: ", 1, RtUtilities::Orange);
    RtUtilities::DebugLog(event_string, RtUtilities::Cyan);
    RtUtilities::DebugLog(" on object called: ", RtUtilities::Orange); 
    RtUtilities::DebugLog(sender.GetName() + "\n", RtUtilities::Default);

    if (event == ObjectDeleted){
        RemoveObjectFromManager(sender.GetObjectId());
    }
}


Object * ObjectManager::GetObject(unsigned int object_id) const{
    return _objectList.at(object_id);
}
Object * ObjectManager::GetObject(std::string object_name) const{
    std::map<unsigned int, Object *> listToIterate = _objectList;
    std::map<unsigned int, Object *>::iterator iterate = listToIterate.begin();
    while (iterate != listToIterate.end())
    {
        if(iterate->second->GetName() == object_name ){
            return iterate->second;
        }
        iterate++;
    }
    return nullptr;
}

unsigned int ObjectManager::SetNewId(){
    _current_id++;
    return _current_id;
}
unsigned int ObjectManager::GetCurrentId() const{
    return _current_id;
}


//returns id if successful, 0 if failed
unsigned int ObjectManager::AddObjectToManager(Object &objectToAdd){
    _objectList[SetNewId()] = &objectToAdd;
    RtUtilities::DebugLog("Object " +  _objectList.at(GetCurrentId())->GetName()
    + " with type: " + std::to_string(_objectList.at(GetCurrentId())->GetObjectType())
    + " added;\n", 2);
    return GetCurrentId();
}

int ObjectManager::RemoveObjectFromManager(unsigned int object_id){
    int erased_count = _objectList.erase(object_id);
    RtUtilities::DebugLog("removing object id:" +  std::to_string(object_id) + " is: " + (erased_count>0?"succeed":"failed") + "\n", 3);
    if(erased_count > 0){
        return 1;
    } else {
        return -1;
    }
}

void ObjectManager::DeleteAllObjects(){
    RtUtilities::DebugLog("\nDeleting objects from Object Manager\n");
    if(_objectList.size() > 0){
        RtUtilities::DebugLog("\nnumber of objects to delete: " + std::to_string(_objectList.size()) + " \n");

        std::map<unsigned int, Object *> listToIterate = _objectList;
        std::map<unsigned int, Object *>::iterator iterate = listToIterate.begin();
        int i = 0;
        while (iterate != listToIterate.end())
        {
            if(iterate->second != nullptr){
//            if(iterate->second != nullptr && iterate->second->GetObjectType() != ObjectTypes::Scenes){
                RtUtilities::DebugLog("#:" + std::to_string(iterate->first) + ", Deleting object: " + iterate->second->GetName() + "\n", 3);
                delete iterate->second;
                iterate->second = nullptr;
            }
            iterate++;
            i++;
        }
        RtUtilities::DebugLog("\nObjects left:" +  std::to_string(_objectList.size()) + "\n");

        _objectList.clear();
    }

    RtUtilities::DebugLog( "\nAll objects are deleted;\n\n");
}
