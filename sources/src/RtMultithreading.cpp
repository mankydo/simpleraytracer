#include "RtMultithreading.h"
#include "RtUtilities.h"
#include <chrono>

image RaytraceMultithread::RenderMultithreaded(IRenderer * renderer, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters)
{
    bool debugOutput = false;
    image resultImage = image(in_renderParameters.width, in_renderParameters.height);
    resultImage.fillWithColor(color4(0));
    resultImage.setSamples(0);

    int samplesToCount = in_renderParameters.samples;

    int totalSamplesCounted = 0;
    int numberOfThreads = std::thread::hardware_concurrency() - 1;
    if(numberOfThreads <= 1){
        renderer->render(resultImage, scene, renderCamera, in_renderParameters);
        return resultImage;
    }
//    if(debugOutput)
        std::cout << "Number of available threads is: " << numberOfThreads + 1 << "\n";
        std::cout << "Number of threads in use is: " << numberOfThreads << "\n";

    std::vector<ThreadFutures> threadFutureList = std::vector<ThreadFutures>();

    int samplesPool = samplesToCount;
// to be calculated more precisely later based on how many samples can be rendered 
//   to get a reasonable realtime fps(3-10 or something like this)
    int samplesPerThread = 3;
    int maxThreads = 0;
    int numberOfThreadsProcceded = 0;

    // Record start time
    auto render_start = std::chrono::high_resolution_clock::now();
    std::time_t render_start_time = std::chrono::system_clock::to_time_t(render_start);

    while (totalSamplesCounted < samplesToCount)
    {
        if(samplesPool > 0){
            if((int)threadFutureList.size() < numberOfThreads) {
                int samplesToCalculate = std::min(samplesPerThread, samplesPool);

                RenderParameters renderPerThreadParams = in_renderParameters;
                renderPerThreadParams.samples = samplesToCalculate;
                renderPerThreadParams.normalizeResult = false;

                std::packaged_task<image(int, IRenderer *, const Scene &, const Camera &, RenderParameters)> newTask(&ExecuteRender);
                std::future<image> newFuture = newTask.get_future();

                std::thread newThread(std::move(newTask), 
                    samplesToCalculate,
                    renderer,
                    std::ref(scene),
                    std::ref(renderCamera),
                    renderPerThreadParams
                );

                ThreadFutures newThreadFuture = {std::move(newThread), std::move(newFuture)};
                threadFutureList.push_back(std::move(newThreadFuture));
                numberOfThreadsProcceded++;

                samplesPool -= samplesToCalculate; 
            }
        }
 
         for( auto it = threadFutureList.begin(); it != threadFutureList.end();){
            if( it->m_future.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready){
                if(it->m_thread.joinable()){
                    it->m_thread.join();
                    if(it->m_future.valid()) {
                        image renderedImageByAThread = it->m_future.get();
                        totalSamplesCounted += renderedImageByAThread.GetSamples();
                        resultImage = resultImage + renderedImageByAThread;
                        if(in_renderParameters.printProgress)
                            RtUtilities::PrintProgress((float)totalSamplesCounted / samplesToCount);
                        it = threadFutureList.erase(it);
                    }
                    if(debugOutput)
                        std::cout << "threads number: " <<  threadFutureList.size() <<" total is: " << totalSamplesCounted << "\n";
                }
            } else {
                ++it;
            }
        }      
        maxThreads = maxThreads < (int)threadFutureList.size() ? threadFutureList.size() : maxThreads;
    }

    // Record end time
    auto render_finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> render_duration = render_finish - render_start;
    std::time_t render_finish_time = std::chrono::system_clock::to_time_t(render_finish);

    resultImage.setrenderstats(render_duration.count(), render_start_time, render_finish_time);
    resultImage.setSamples(totalSamplesCounted);
    if(in_renderParameters.normalizeResult)
        resultImage.NormalizeBySamples();

    if(debugOutput)
        std::cout << "\n max threads was: " << maxThreads
            << "\ntotal samples is: " << totalSamplesCounted 
            << " and total number of threads is " << numberOfThreadsProcceded
            << " and threads in a map: " << threadFutureList.size() << " \n";

    return resultImage;
}

image RaytraceMultithread::ExecuteRender(int samples, IRenderer * renderer, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters)
{
    image imageToRender = image(in_renderParameters.width, in_renderParameters.height);
    in_renderParameters.samples = samples;
    in_renderParameters.printProgress = false;
    renderer->render(imageToRender, scene, renderCamera, in_renderParameters);
    
//    RtUtilities::DebugLog("thead finished it's work :: ", 2);
    
    return imageToRender;
}