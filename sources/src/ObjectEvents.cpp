#include <algorithm>
#include <iostream>
#include "ObjectEvents.h"
#include "RtUtilities.h"

void ObjectEvents::AddListener(IObjectObverver &observerToSubscribe)
{
    std::vector<IObjectObverver *>::iterator iterate = std::find(
        _listOfSubscribers.begin(),
        _listOfSubscribers.end(),
        &observerToSubscribe);

    if(iterate == _listOfSubscribers.end()){
        _listOfSubscribers.push_back(&observerToSubscribe);
        std::cout <<  _listOfSubscribers.size() << " observers are subscribed now\n";
    }
}
void ObjectEvents::RemoveListener(IObjectObverver &observerToUnsubscribe)
{
    std::vector<IObjectObverver *>::iterator iterate = std::find(
        _listOfSubscribers.begin(),
        _listOfSubscribers.end(),
        &observerToUnsubscribe);

    if(iterate != _listOfSubscribers.end()){
        _listOfSubscribers.erase(iterate);
        RtUtilities::DebugLog("Object has been unsubscribed\n", 1);
    }
}
void ObjectEvents::Invoke(ObjectEventsEnum event, Object &sender)
{
//    std::vector<IObjectObverver *>::iterator iterate = _listOfSubscribers.begin();
    for( auto sendTo : _listOfSubscribers){
        sendTo->GetObjectMessage(event, sender);
    }
}
