#include "image.h" 


image::image(int in_width, int in_height, color4 in_color, bool array){
	//make two implementations:
	// one with one dimentional array
	// and the other one with two dimentional vectors
	if(array){
//		pixels = new vec3 [width * height];
		width = in_width;
		height = in_height;
		pixels.resize(width*height);
		this->fillWithColor(in_color);
	} else {
		 std::printf("vector pixels are not implemented yet\n");
	}
}

image::~image(){
//	delete[] pixels;
	pixels.clear();
}

int image::clampInt(int x, int clamp_low, int clamp_high) const{
	x = x< clamp_low ? clamp_low : x;
	x = x>=clamp_high ? clamp_high - 1 : x;

	return x;
}


color4 image::getPixelRaw(int x, int y) const{
	x = clampInt(x, 0, width);
	y = clampInt(y, 0, height);

	return pixels.at(y * width + x);
}

color4 image::getPixelGammaCorrected(int x, int y) const{
	x = clampInt(x, 0, width);
	y = clampInt(y, 0, height);

	color4 outputColor = pixels.at(y * width + x);

	return color4(
			std::pow(outputColor.r(), 1/ gamma),
			std::pow(outputColor.g(), 1/ gamma),
			std::pow(outputColor.b(), 1/ gamma),
			std::pow(outputColor.a(), 1/ gamma)
		);
}

void image::setpixel(int x, int y, color4 color){
	x = clampInt(x, 0, width);
	y = clampInt(y, 0, height);

//	vec3 clr = vec3(color.r(), color.g() * 0.1, color.b());
//	pixels.at(y * width + x) = color_out;

	pixels.at(y * width + x) = color4( 
		std::max((float)0, color.r()), 
		std::max((float)0, color.g()),
		std::max((float)0, color.b()),
		std::max((float)0, color.a()) 
	);
}

imageRenderStats image::getrenderstats(){
	return renderStats;
}

double image::getrendertime(){
	return this->renderStats.rendertime;
}
double image::getrenderstart(){
	return this->renderStats.renderstart;
}
double image::getrenderend(){
	return this->renderStats.renderend;
}
    
void image::setrenderstats(double in_rendertime, double in_renderstart, double in_renderend){
	this->renderStats.rendertime = in_rendertime;
	if(in_renderstart != -1)
		this->renderStats.renderstart = in_renderstart;
	if(in_renderend != -1)
		this->renderStats.renderend = in_renderend;
}
void image::setrenderstats(imageRenderStats in_renderstats){
	this->renderStats = in_renderstats;
}

int image::getwidth() const{
	return width;
}

int image::getheight() const{
	return height;
}

void image::clear(){
	fillWithColor(color4(0,0,0, 1));
}

void image::fillWithColor(color4 color){
	for(color4 &pixel : pixels){
		pixel = color;
	}
}

int image::GetSamples(){
	return this->samples;
}

void image::setSamples(int samples){
	this->samples = std::max(1, samples);
}


void image::NormalizeBySamples(int samples){
	*this = *this / std::max(1.0, (double)samples);
}
void image::NormalizeBySamples(){
	*this = *this / this->samples;
	this->setSamples(1);
}

image image::Crop(int in_newWidth, int in_newHeight){
    int newWidth = std::min(std::max(in_newWidth, 0), this->getwidth());
    int newHeight = std::min(std::max(in_newHeight, 0), this->getheight());

    image result = image(newWidth, newHeight);
	for(int x = 0; x < newWidth; x++){
		for(int y = 0; y < newHeight; y++ ){
			result.setpixel(x, y, this->getPixelRaw(x, y));
		}
	}
	return result;
}


