#include <iostream>
#include "Object.h"
#include "ObjectManager.h"
#include "RtUtilities.h"

ObjectEvents Object::events;

Object::Object(std::string name){
    this->objectName = name;
    this->events.Invoke(ObjectCreated, *this);
    unsigned int temp_id = ObjectManager::Instance()->AddObjectToManager(*this);
    if(temp_id != 0){
        this->object_id = temp_id;
    }
}

Object::Object(std::string name, ObjectTypes type){
    this->objectName = name;
    this->object_type = type;
    this->events.Invoke(ObjectCreated, *this);
    unsigned int temp_id = ObjectManager::Instance()->AddObjectToManager(*this);
    if(temp_id != 0){
        this->object_id = temp_id;
    }
}


Object::~Object(){
    this->events.Invoke(ObjectDeleted, *this);
    RtUtilities::DebugLog("Object "+ this->objectName + " :: id:" + std::to_string(this->object_id) + " got deleted\n", RtUtilities::Orange);
}
ObjectTypes Object::GetObjectType() const{
    return this->object_type;
}

unsigned int Object::GetObjectId() const{
    return this->object_id;
}

std::string Object::GetName() const{
    return this->objectName;
}
int Object::SetName(std::string newName){
    this->objectName = newName;
    return 1;
}
int Object::SetObjectType(ObjectTypes newObjectType){
    this->object_type = newObjectType;
    return 1;
}