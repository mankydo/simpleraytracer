#include "RtMath.h"
#include <cmath>


double RtMath::random_double(){
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    static std::mt19937 generator;
    static std::function<double()> rand_generator = 
        std::bind(distribution, generator);
    return rand_generator();
}

double RtMath::random_double(double min, double max) {
    // Returns a random real in [min,max).
    return min + (max-min)*random_double();
}

vec3 RtMath::RandomInUnitSphere(){
    while (true) 
    {
        vec3 p = RtMath::RandomVec3(-1, 1);
        if(p.squared_length() >= 1) continue;
        return p;
    }    
}

vec3 RtMath::RandomUnitVector(){
    double angle = RtMath::random_double(0, 2 * pi);
    double z = RtMath::random_double(-1, 1);
    double radius = std::sqrt(1 - z*z);
    return vec3(radius * std::cos(angle), radius * std::sin(angle), z);
}

bool RtMath::PlaneRayIntersection(const vec3 planeOrigin, const vec3 planeNormal, const ray & rayToIntersect, vec3 & pointOfIntersection)
{
    bool result = false;
    double distanceOnRayResult = 0;

    result = PlaneRayIntersection(planeOrigin, planeNormal, rayToIntersect, distanceOnRayResult);
    if(result){
        pointOfIntersection = rayToIntersect.origin() + rayToIntersect.direction() * distanceOnRayResult;
    };
    return result;
}

bool RtMath::PlaneRayIntersection(const vec3 planeOrigin, const vec3 planeNormal, const ray & rayToIntersect, double & distanceOnRayResult)
{
    bool result = false;

    vec3 normalizedPlaneNormal = unit_vector(planeNormal);
//    vec3 normalizedPlaneNormal = planeNormal;
    float dotRayPlane = dot(unit_vector(rayToIntersect.direction()), normalizedPlaneNormal);
    if(dotRayPlane != 0) 
    {
        distanceOnRayResult = dot(planeOrigin - rayToIntersect.origin(), normalizedPlaneNormal) / dotRayPlane;
        result = true;
    } else 
    {
        result = false;
    }

    return result;
}

bool RtMath::XYQuadRayIntersection(const ray & rayToIntersect, 
    const float & xMin, const float & xMax, 
    const float & yMin, const float & yMax, 
    double & distanceOnRayResult)

{
    bool result = false;

    if(rayToIntersect.direction().z() <= 0 ) 
    {
        result = false;
    } 
    else {
   
    //get x from xz-intersection where z = 0
        float xP = rayToIntersect.origin().x() - ( rayToIntersect.direction().x() / rayToIntersect.direction().z() ) * rayToIntersect.origin().z();

        if(xP > xMin && xP < xMax) {

        //get y from yz-intersection where z = 0
            float yP = rayToIntersect.origin().y() - ( rayToIntersect.direction().y() / rayToIntersect.direction().z() ) * rayToIntersect.origin().z();
    
            if(yP > yMin && yP < yMax) {

            //get intersection point P from (x,y,0)
                vec3 hitPoint = vec3(xP, yP, 0);

            //get distance on a ray from magnitude of a vector (RayOrigin - P)
                distanceOnRayResult = (rayToIntersect.origin() - hitPoint).length();
                result = true;
            }
        }
    }
    return result;

}

bool RtMath::YZQuadRayIntersection(const ray & rayToIntersect, 
    const float & yMin, const float & yMax, 
    const float & zMin, const float & zMax, 
    double & distanceOnRayResult)

{
    bool result = false;

    if(rayToIntersect.direction().x() <= 0 ) 
    {
        result = false;
    } 
    else {
   
    //get y from xy-intersection where x = 0
        float yP = rayToIntersect.origin().y() - ( rayToIntersect.direction().y() / rayToIntersect.direction().x() ) * rayToIntersect.origin().x();

        if(yP > yMin && yP < yMax) {

        //get z from xz-intersection where x = 0
            float zP = rayToIntersect.origin().z() - ( rayToIntersect.direction().z() / rayToIntersect.direction().x() ) * rayToIntersect.origin().x();
    
            if(zP > zMin && zP < zMax) {

            //get intersection point P from (x,y,0)
                vec3 hitPoint = vec3(yP, zP, 0);

            //get distance on a ray from magnitude of a vector (RayOrigin - P)
                distanceOnRayResult = (rayToIntersect.origin() - hitPoint).length();
                result = true;
            }
        }
    }
    return result;

}

bool RtMath::XZQuadRayIntersection(const ray & rayToIntersect, 
    const float & xMin, const float & xMax, 
    const float & zMin, const float & zMax, 
    double & distanceOnRayResult)

{
    bool result = false;

    if(rayToIntersect.direction().y() <= 0 ) 
    {
        result = false;
    } 
    else {
   
    //get x from xy-intersection where y = 0
        float xP = rayToIntersect.origin().x() - ( rayToIntersect.direction().x() / rayToIntersect.direction().y() ) * rayToIntersect.origin().y();

        if(xP > xMin && xP < xMax) {

        //get z from yz-intersection where y = 0
            float zP = rayToIntersect.origin().z() - ( rayToIntersect.direction().z() / rayToIntersect.direction().y() ) * rayToIntersect.origin().y();
    
            if(zP > zMin && zP < zMax) {

            //get intersection point P from (x,y,0)
                vec3 hitPoint = vec3(xP, zP, 0);

            //get distance on a ray from magnitude of a vector (RayOrigin - P)
                distanceOnRayResult = (rayToIntersect.origin() - hitPoint).length();
                result = true;
            }
        }
    }
    return result;

}

vec3 RtMath::RandomPointOnADiskXY(float radius){
    float length =  std::sqrt(random_double()) * radius;
    float angle = random_double(0, 2 * pi);
    return vec3(std::cos(angle) * length, std::sin(angle) * length, 0);  
}

vec3 RtMath::RandomPointOnADiskXY()
{
    return RtMath::RandomPointOnADiskXY(1);
}

vec3 RtMath::RandomPointOnANgon(float radius, int numberOfLeafs, float rotateAngleInRadians){
//generates an n-vertices polygon within a circle with a given radius

// a basic algorithm is simple: Since any ngon is a bunch of triangles we can 
//   create a triangle with one point at origin and two others on a circle bound,
//   and rotate it by a number of times our desired ngon's edges has. 
//   I.e. if number of leafs/edges = 3, 
//    we divide circle by three and that would give us three equal triangles sharing one point at the origin
//    and we can get any next triangle by rotating the first one by 2pi/number of leafs/edges around the origin


    //a 2-vertices ngon sis not an ngon but a line segment
    if(numberOfLeafs <= 3)
        numberOfLeafs = 3;

    float angleToStepForTriangle = 2 * pi / numberOfLeafs;

    //creating a rotate matrix for a given custom rotation
    float rotationSin1 = std::sin(rotateAngleInRadians);
    float rotationCos1 = std::cos(rotateAngleInRadians);

    Matrix3x3 rotationMatrixFixed = 
            Matrix3x3(  rotationCos1, -1 * rotationSin1, 0,
                        rotationSin1, rotationCos1, 0,
                        0, 0, 1
                    );

    // Creating three points for our original triangle:
    //  one at origin, second one on (radius, 0) and the last one by rotating the second one by 2pi/number of edges.
    vec2 pointA = vec2{0,0};
    vec2 pointB = vec2{1,0} * radius;
    vec2 pointC = vec2{std::cos(angleToStepForTriangle), std::sin(angleToStepForTriangle)} * radius;

    //The magic happens here: to sample any point on a ngon within a circle
    //  we are sampling a random point from a triangle first and then rotate the triangle random number of times(less than number of edges)

    //To find a random point in a triangle first we must define any point within a triangle; 
    // For that we find a random point on an opposite side(B-C) of origin point(point A), 
    // and then find a random point on a line connecting origin and that random point.
    // Since lerp is essentially a line between any two points sliding from 0 to 1, finding a random point is trivial
    vec2 pointD = RtMath::lerp(pointB, pointC, RtMath::random_double(0, 1));

    // the only trick here is since we are sliding inside a triangle, 
    // points closer to origin would be placed much more densier than on the outer radius.
    // To remedy this we must find probability density function(PDF) which happened to be y=2x 
    //   (because at any given uniform intervals of x next y will be 2 times bigger 
    //   therefore an area under y=2x at {0,1} will be exactly 1, i.e. an accumulated probability)
    // But to use it we must find an appropriate cumulative density function(CDF) which happed to be an integral of PDF, y = x^2
    // The last part of the trick is since we actually need to know the distribution of x instead of y(we are randomizing x across the line AD)
    // we are inverting CDF(by swaping x and y axis) and get x = y ^ 2, or y = sqrt(x) and got a neat unform distribution
    vec2 pointToDraw = RtMath::lerp(pointA, pointD, std::sqrt(RtMath::random_double(0, 1)) );

    // the rest is trivial: calculating a rotation matrix with a random angle step
    float angleToRotate = angleToStepForTriangle * (int)(RtMath::random_double(0,numberOfLeafs) + .5);

    float rotationSin = std::sin(angleToRotate);
    float rotationCos = std::cos(angleToRotate);

    Matrix3x3 rotationMatrix = 
        Matrix3x3(  rotationCos, -1 * rotationSin, 0,
                    rotationSin, rotationCos, 0,
                    0, 0, 1
                );

    // and rotate the point in a initial triangle by a descrete steps to uniformly fill the ngon 
    // with triangles randomly rotated around the origin
    pointToDraw = pointToDraw * rotationMatrix * rotationMatrixFixed;

    return pointToDraw.toVec3();
}

vec3 RtMath::RandomPointOnANgon(int numberOfLeafs){
    return RandomPointOnANgon(1, numberOfLeafs, 0);
}


float RtMath::lerp(float const & pointA, float const & pointB, float factor01)
{
//    return pointA * (1 - factor01) + pointB * (factor01);
//    pointA - pointA* factor01 + pointB * factor01
    return pointA  + factor01 * (pointB - pointA);
}

vec2 RtMath::lerp(vec2 const & pointA, vec2 const & pointB, float factor01)
{
//    return vec2{pointA.x  + factor01 * (pointB.x - pointA.x), pointA.y  + factor01 * (pointB.y - pointA.y)};
    return pointA  + factor01 * (pointB - pointA);
}

vec3 RtMath::lerp(vec3 const & pointA, vec3 const & pointB, float factor01)
{
    return pointA  + factor01 * (pointB - pointA);
}

//Rotate a vec3 based on angles provided
vec3 RtMath::RotateByAxis(vec3 const & vec3ToRotate, vec3 axisToRotate){

    return vec3ToRotate * RtMath::Matrices::RotationMatrix(axisToRotate);
}

//Undo the rotate a vec3 based on angles provided
vec3 RtMath::RotateByAxisInverse(vec3 const & vec3ToRotate, vec3 axisToRotate){
// It's actually a transpose of a rotation matrix because in case of rotation: R^(-1) = R^T
// i.e. an inverse matrix is equal to a transposed matrix since it's determinant is equal to 1

    return vec3ToRotate * RtMath::Matrices::Transpose(RtMath::Matrices::RotationMatrix(axisToRotate));
}

vec3 RtMath::UnRotateByAxis(vec3 const & vec3ToRotate, vec3 axisToRotate){

    axisToRotate *= -1;

    float angleToRotate = axisToRotate.z();
    float rotSinZ = std::sin(angleToRotate);
    float rotCosZ = std::cos(angleToRotate);

    angleToRotate = axisToRotate.y();
    float rotSinY = std::sin(angleToRotate);
    float rotCosY = std::cos(angleToRotate);

    angleToRotate = axisToRotate.x();
    float rotSinX = std::sin(angleToRotate);
    float rotCosX = std::cos(angleToRotate);

    Matrix3x3 rotationMatrixZ = 
    Matrix3x3(  rotCosZ, -1 * rotSinZ, 0,
                rotSinZ, rotCosZ, 0,
                0, 0, 1
        );

     Matrix3x3 rotationMatrixY = 
    Matrix3x3(  rotCosY, 0, rotSinY,
                0, 1, 0,
                -1 * rotSinY, 0, rotCosY
        ); 

    Matrix3x3 rotationMatrixX = 
    Matrix3x3(  1, 0, 0, 
                0, rotCosX, -1 *rotSinX,
                0, rotSinX,rotCosX
        ); 

    return vec3ToRotate * rotationMatrixX * rotationMatrixY * rotationMatrixZ;
}

Matrix3x3 RtMath::Matrices::RotationMatrix(const vec3 & axisToRotateInRadians){
    float angleToRotate = axisToRotateInRadians.z();
    float rotSinZ = std::sin(angleToRotate);
    float rotCosZ = std::cos(angleToRotate);

    angleToRotate = axisToRotateInRadians.y();
    float rotSinY = std::sin(angleToRotate);
    float rotCosY = std::cos(angleToRotate);

    angleToRotate = axisToRotateInRadians.x();
    float rotSinX = std::sin(angleToRotate);
    float rotCosX = std::cos(angleToRotate);

/*     Matrix3x3 rotationMatrixZ = 
    Matrix3x3(  rotationCosZ, -1 * rotationSinZ, 0,
                rotationSinZ, rotationCosZ, 0,
                0, 0, 1
        );

     Matrix3x3 rotationMatrixY = 
    Matrix3x3(  rotationCosY, 0, rotationSinY,
                0, 1, 0,
                -1 * rotationSinY, 0, rotationCosY
        ); 

    Matrix3x3 rotationMatrixX = 
    Matrix3x3(  1, 0, 0, 
                0, rotationCosX, -1 *rotationSinX,
                0, rotationSinX,rotationCosX
        ); 
//    return vec3ToRotate * rotationMatrixZ * rotationMatrixY * rotationMatrixX;
*/

// This matrix is actually a multiplication product of three matrices above, 
//      rotating by one axis at the time in a particular order: first by X, then by Y and lastly by Z axis.
// To be able to "unrotate" the matrix there are two methods:
//  - One being rotating by the same angles backward, i.e. 
//      multiply an angles tuple by -1 and create a new matrix with the rotation order inverted: first by Z, then by Y, lastly by X.
//      Something like this: RotMatrix = rotMatrix(-X) * rotMatrix(-Y) * rotMatrix(-Z);
//  - Second being by inversing(or transposing in case of symmethrical matrices because it's determinant is equal to 1)

    Matrix3x3 rotationMatrix =
    Matrix3x3(  rotCosZ * rotCosY, rotCosZ * rotSinY * rotSinX - rotSinZ * rotCosX, rotCosZ * rotSinY * rotCosX + rotSinZ * rotSinX,
                rotSinZ * rotCosY, rotSinZ * rotSinY * rotSinX + rotCosZ * rotCosX, rotSinZ * rotSinY * rotCosX - rotCosZ * rotSinX,
                -1 * rotSinY,       rotCosY * rotSinX,                              rotCosY * rotCosX
    );
    return rotationMatrix;
}

Matrix3x3 RtMath::Matrices::Inverse(const Matrix3x3 & matrixToInverse){
    Matrix3x3 result = Matrix3x3();

    Matrix3x3 minorMatrix = Matrix3x3();

//find a matrix of minors
    for(int i = 0; i < 3; i++){
        for( int j = 0; j < 3; j++){
            minorMatrix.e[i][j] = RtMath::Matrices::DeterminantForElement(matrixToInverse, i, j);
        }
    }

// applying a checkerboard pattern of minuses to get a matrix of cofactors
    for(int i = 0; i < 9; i++){
//        minorMatrix.e[i - (int)((i) / 3) * 3][(int)(i / 3)] *= (i % 2 == 0 ? 1 : -1);
        minorMatrix.e[i - (int)((i) / 3) * 3][(int)(i / 3)] *= 1  - (i % 2) * 2;
    }

// finding a determinant
    float determinant = matrixToInverse.e[0][0] * minorMatrix.e[0][0] 
                        + matrixToInverse.e[1][0] * minorMatrix.e[1][0] 
                        + matrixToInverse.e[2][0] * minorMatrix.e[2][0]
                    ;

// transposing a matrix of cofactors to get an adjoint matrix
    minorMatrix = RtMath::Matrices::Transpose(minorMatrix);

// multiplying a transposed matrix of cofactors by a determinant
    result = determinant * minorMatrix;

    return result;
}

Matrix3x3 RtMath::Matrices::Transpose(const Matrix3x3 & matrixToTranspose){
    Matrix3x3 matrixTransposed = Matrix3x3();

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            matrixTransposed.e[j][i] = matrixToTranspose.e[i][j];
        }
    }
    return matrixTransposed;
}

float RtMath::Matrices::DeterminantForElement(const Matrix3x3 & matrixToCalculate, int column, int row){
    float determinant = 0;

    std::vector<float> elementsToCalculate = std::vector<float>();

    for(int i = 0; i < 3; i++){
        if(i != column){
            for(int j = 0; j < 3; j++){
                if(j != row){
                    elementsToCalculate.push_back(matrixToCalculate.e[i][j]);
                }
            }
        }
    }

    determinant = elementsToCalculate[0] * elementsToCalculate[3] - 
                elementsToCalculate[1] * elementsToCalculate[2];

    return determinant;
}
