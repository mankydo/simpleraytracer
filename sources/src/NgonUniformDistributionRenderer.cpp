#include <chrono>  // for high_resolution_clock
#include <iostream>
#include "image.h"
#include "RtMath.h"
#include "RtUtilities.h"
#include "NgonUniformDistributionRenderer.h"
#include "matrix3x3.h"


void NgonTestRenderer::render(image &image, int inSamples)
{
    int nx = image.getwidth();
    int ny = image.getheight();
    vec2 dimensions = vec2{(float)nx, (float)ny};

    vec2 pointA = vec2{0,0};
    vec2 pointB = vec2{0.7,0.1};
    vec2 pointC = vec2{0.8,0.7};

    image.fillWithColor(color4(.8,.8,.8,1));

    color4 ngonColor = color4(.1,.5,.1,1);
    color4 ngonColor2 = color4(.8,.3,.3,1);
    color4 ngonColor3 = color4(.1,.3,.8,1);
    color4 randomColor = color4(.1,.5,.9,1);
    color4 ngonLineColor = color4(.7,.2,.5,1);

    // Record start time
    auto render_start = std::chrono::high_resolution_clock::now();
    std::time_t render_start_time = std::chrono::system_clock::to_time_t(render_start);

    pointA = pointA * dimensions;
    pointB = pointB * dimensions;
    pointC = pointC * dimensions;

    this->DrawLine(image, pointA, pointB, ngonLineColor);
    this->DrawLine(image, pointB, pointC, ngonLineColor);
    this->DrawLine(image, pointC, pointA, ngonLineColor);

    for(int i = 0; i < inSamples; i++){
        
        int x = RtMath::random_double(0, 1) * nx;
        vec2 pointD = RtMath::lerp(pointB, pointC, RtMath::random_double(0, 1));

        int y = RtMath::random_double(0, 1) * ny;
        vec2 pointX = RtMath::lerp(pointA, pointD, std::sqrt(RtMath::random_double(0, 1)) );

        image.setpixel(pointX.x, pointX.y, randomColor);
        image.setpixel(x, y, ngonColor);
    }

    DrawNgon(image, vec2{nx/2,ny/2}, 200, 7, ngonColor2, inSamples);

    DrawNgonShared(image, vec2{nx/2,ny/2}, 200, 3, .6, ngonColor3, inSamples);

    // Record end time
    auto render_finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> render_duration = render_finish - render_start;
    std::time_t render_finish_time = std::chrono::system_clock::to_time_t(render_finish);

    image.setrenderstats(render_duration.count(), render_start_time, render_finish_time);

}

float NgonTestRenderer::GetYOfLine(float const & x, vec2 const & pointA, vec2 const & pointB){
    float a = (pointB.y - pointA.y) / (pointB.x - pointA.x);
    return a * (x - pointA.x) + pointA.y;
}

void NgonTestRenderer::DrawLine(image & inImage, vec2 pointA, vec2 pointB, color4 lineColor){

    if(pointA.x == pointB.x)
    {
        if(pointA.y > pointB.y)
        {
            vec2 swapPoint = pointA;
            pointA = pointB;
            pointB = swapPoint;
        }

        for(int i =pointA.y; i <= pointB.y; i++)
        {
            inImage.setpixel(pointA.x, i, lineColor);
        }
    } 
    else {
    
        if(pointA.x > pointB.x)
        {
            vec2 swapPoint = pointA;
            pointA = pointB;
            pointB = swapPoint;
        }

        for(int i = pointA.x; i < pointB.x; i++)
        {
            int yAB = this->GetYOfLine(i, pointA, pointB);
            if( i > pointA.x && i < pointB.x){
                inImage.setpixel(i, yAB, lineColor);
            }
        }
    }
}

void NgonTestRenderer::DrawNgon(image & inImage, vec2 position, float radius, int numberOfLeafs, color4 ngonColor, int samples){
// A = 0
// calculate B and C based on number of leafs with unit radius
// loop of samples
// generate a random point R
// get random leaf( get random number of leaf and rotate R using rotation matrix)
// move R to it's final position using matrix(probably these two matrices should be joined together)
// end loop

    Matrix3x3 translateMatrix = 
        Matrix3x3(  1, 0, 0,
                    0, 1, 0,
                    position.x, position.y, 1
                );

    float angleToRotate = .6;
    float rotationSin = std::sin(angleToRotate);
    float rotationCos = std::cos(angleToRotate);

    Matrix3x3 rotationMatrixFixed = 
            Matrix3x3(  rotationCos, -1 * rotationSin, 0,
                        rotationSin, rotationCos, 0,
                        0, 0, 1
                    );
    
    Matrix3x3 translateRotateMatrix = rotationMatrixFixed * translateMatrix;


    if(numberOfLeafs <= 3)
        numberOfLeafs = 3;

    float angleToStepForTriangle = 2 * pi / numberOfLeafs;

    vec2 pointA = vec2{0,0};
    vec2 pointB = vec2{1,0} * radius;
    vec2 pointC = vec2{std::cos(angleToStepForTriangle), std::sin(angleToStepForTriangle)} * radius;


    vec2 generatedPoints[samples];

    for(int i=0; i <= samples; i++){
        vec2 pointD = RtMath::lerp(pointB, pointC, RtMath::random_double(0, 1));
        vec2 pointToDraw = RtMath::lerp(pointA, pointD, std::sqrt(RtMath::random_double(0, 1)) );

        float angleToRotate = angleToStepForTriangle * (int)(RtMath::random_double(0,numberOfLeafs) + .5);

        float rotationSin = std::sin(angleToRotate);
        float rotationCos = std::cos(angleToRotate);

        Matrix3x3 rotationMatrix = 
            Matrix3x3(  rotationCos, -1 * rotationSin, 0,
                        rotationSin, rotationCos, 0,
                        0, 0, 1
                    );

        Matrix3x3 transformMatrix = rotationMatrix * translateRotateMatrix;

//        pointToDraw = pointToDraw * rotationMatrix;
//        pointToDraw = pointToDraw * translateMatrix;

        pointToDraw = pointToDraw * transformMatrix;

        generatedPoints[i] = pointToDraw;

        inImage.setpixel(pointToDraw.x, pointToDraw.y, ngonColor);
    }
}

void NgonTestRenderer::DrawNgonShared(image & inImage, vec2 position, float radius, int numberOfLeafs, float rotateAngleInRadians, color4 ngonColor, int samples){

    Matrix3x3 translateMatrix = 
        Matrix3x3(  1, 0, 0,
                    0, 1, 0,
                    position.x, position.y, 1
                );

    float rotationSin = std::sin(rotateAngleInRadians);
    float rotationCos = std::cos(rotateAngleInRadians);

    Matrix3x3 rotationMatrixFixed = 
            Matrix3x3(  rotationCos, -1 * rotationSin, 0,
                        rotationSin, rotationCos, 0,
                        0, 0, 1
                    );
    
    Matrix3x3 translateRotateMatrix = rotationMatrixFixed * translateMatrix;


    for(int i=0; i <= samples; i++){

        vec3 pointToDraw3 = RtMath::RandomPointOnANgon(radius, numberOfLeafs,rotateAngleInRadians);

//        pointToDraw3 = (pointToDraw3 + vec3(0,0,1)) * translateRotateMatrix;
        pointToDraw3 = (pointToDraw3 + vec3(0,0,1)) * translateMatrix;

        inImage.setpixel(pointToDraw3.x(), pointToDraw3.y(), ngonColor);

    }
}

