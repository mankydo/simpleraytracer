#include "RtUtilities.h"
#include <iostream>
#include "GlobalVariables.h"
#include <algorithm>
#include <cctype>

void RtUtilities::PrintProgress(float progress01){

    int barWidth = 70;

    std::cout << "[";
    int pos = (float)(barWidth * progress01);
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";
    }
    std::cout << "] " << int(progress01 * 100.0) << " %\r";
    std::cout.flush();
}

void RtUtilities::Log(const std::string & message, int indent, LogColor color){

//    std::string messageColor = GetLogColor(color);
    std::string indentString = "  ";
    std::string indentTotal = "";
    if(indent > 0){
        for(int i = 0; i < indent; i++){
            indentTotal += indentString;
        }
    }
    
    std::string finalMessage = indentTotal + "\033[" + std::to_string(color) + "m" + message + "\033[0m";
   
    std::cout << finalMessage;
}

void RtUtilities::Log(const std::string & message, int indent){
    Log(message, indent, LogColor::Default);
}

void RtUtilities::Log(const std::string & message, LogColor color){
    Log(message, 0, color);
}

void RtUtilities::Log(const std::string & message){
    Log(message, 0, LogColor::Default);
}

void RtUtilities::DebugLog(const std::string & message){
    if(g_DebugLog){
        Log(message, LogColor::Grey);
    }
}

void RtUtilities::DebugLog(const std::string & message, int indent){
    if(g_DebugLog){
        Log(message, indent, LogColor::Grey);
    }
}

void RtUtilities::DebugLog(const std::string & message, LogColor color){
    if(g_DebugLog){
        Log(message, color);
    }
}

void RtUtilities::DebugLog(const std::string & message, int indent, LogColor color){
    if(g_DebugLog){
        Log(message, indent, color);
    }
}


void RtUtilities::StringToLowercase(std::string & stringToConvert){
    std::transform(stringToConvert.begin(),stringToConvert.end(), stringToConvert.begin(),
    [](unsigned char c){return std::tolower(c);}
    );
}

void RtUtilities::TrimWhitespaces(std::string & stringToConvert){
    size_t first = stringToConvert.find_first_not_of(' ');
    first = first == std::string::npos ? 0 : first;
    size_t last = stringToConvert.find_last_not_of(' ');
    last = last == std::string::npos ? stringToConvert.size() - 1 : last;

    stringToConvert = stringToConvert.substr(first, (last - first + 1));
}



int RtUtilities::ConvertPropertyToInt(const std::string & property, bool &convertSuccessful){

    RtUtilities::DebugLog("testing: " + property + " for Int; \n", 2, RtUtilities::LogColor::Green);

    convertSuccessful = true;
    int intProperty = 0;
    std::string errorCode = "";

    try
    {
        intProperty = std::stoi(property);
    }
    catch(const std::invalid_argument& e)
    {
        errorCode = (std::string)e.what();
        convertSuccessful = false;
    }

    if(!convertSuccessful){
        intProperty = 0;
        RtUtilities::DebugLog("ERROR: " + errorCode + " conversion to INT of property '" + property + "' failed; check your spelling. \n", RtUtilities::Red);
    } else {
        convertSuccessful = true;
    }

    return intProperty;
}

float RtUtilities::ConvertPropertyToFloat(const std::string & property, bool &convertSuccessful){

    std::string propertyTrimmed = property;
    RtUtilities::TrimWhitespaces(propertyTrimmed);

    RtUtilities::DebugLog("testing: " + propertyTrimmed + " for float; \n", 2, RtUtilities::LogColor::Green);

    convertSuccessful = true;
    float floatProperty = 0;
    std::string errorCode = "";

    if(propertyTrimmed.find_first_not_of("-0123456789.") == std::string::npos)
    {
        try
        {
            floatProperty = std::stof(propertyTrimmed);
        }
        catch(const std::invalid_argument& e)
        {
            errorCode = (std::string)e.what();
            convertSuccessful = false;
        }
    } else {
        convertSuccessful = false;
    }

    if(!convertSuccessful){
        floatProperty = 0;
        RtUtilities::DebugLog("ERROR: " + errorCode + " conversion to FLOAT of property '" + propertyTrimmed + "' failed; check your spelling. \n", RtUtilities::Red);
        convertSuccessful = false;
    } else {
        convertSuccessful = true;
    }

    return floatProperty;
}


std::vector<float> RtUtilities::ConvertMultivecPropertyToVecSized(const std::string & property, const int vecSize, bool &convertSuccessful)
{
    int parsefloatGroups = vecSize;
    std::vector<float> result = std::vector<float>();
    bool cummulativeConvertSuccess = true;

    RtUtilities::DebugLog("Testing: " + property + " for vec[" + std::to_string(parsefloatGroups) + "]; \n", 1, RtUtilities::LogColor::Green);

    convertSuccessful = true;

    float tempFloats[parsefloatGroups] = {};
    std::size_t commaPlacements[parsefloatGroups + 1] = {};
    commaPlacements[0] = 0;
    commaPlacements[parsefloatGroups] = property.size();
    RtUtilities::DebugLog("______commaPos[0]: " + std::to_string(commaPlacements[0]) + "\n", 4, RtUtilities::Grey);


    for(int i = 1; i < parsefloatGroups; i++){
        commaPlacements[i] = property.find(",", commaPlacements[i-1] + 1);
        RtUtilities::DebugLog("______commaPos[" + std::to_string(i) + "]: " + std::to_string(commaPlacements[i]) + "\n", 4, RtUtilities::Grey);
        if(commaPlacements[i] == std::string::npos)
            convertSuccessful = false;
        else {
            std::string tempValue = property.substr(commaPlacements[i-1], commaPlacements[i] - commaPlacements[i-1]);
            RtUtilities::DebugLog("______positions are: " + std::to_string(commaPlacements[i-1]) + ":" + std::to_string(commaPlacements[i]) + "\n", 4, RtUtilities::Grey);
            RtUtilities::DebugLog("______property is: " + tempValue + "\n", 4, RtUtilities::Grey);
            tempFloats[i-1] = ConvertPropertyToFloat(tempValue, convertSuccessful);
            std::string convertStatus = convertSuccessful?"true":"false";
            RtUtilities::DebugLog("______result is: " + convertStatus + "\n", 4, RtUtilities::Grey);
            commaPlacements[i] += 1;
        }
        cummulativeConvertSuccess = cummulativeConvertSuccess & convertSuccessful;
    }
    if(cummulativeConvertSuccess){
        RtUtilities::DebugLog("______commaPos[" + std::to_string(parsefloatGroups) + "]: " + std::to_string(commaPlacements[parsefloatGroups]) + "\n", 4, RtUtilities::Grey);
        std::string tempValue = property.substr(commaPlacements[parsefloatGroups - 1], commaPlacements[parsefloatGroups] - commaPlacements[parsefloatGroups - 1]);
        tempFloats[parsefloatGroups - 1] = ConvertPropertyToFloat(tempValue, convertSuccessful);
    }
    
    cummulativeConvertSuccess = cummulativeConvertSuccess & convertSuccessful;

    if(cummulativeConvertSuccess){
        for(int i = 0; i < parsefloatGroups; i++){
            result.push_back(tempFloats[i]);
        }
    } else {
        convertSuccessful = false;
    }

    return result;
}


vec3 RtUtilities::ConvertPropertyToVec3(const std::string & property, bool &convertSuccessful){

    int parsefloatGroups = 3;
    convertSuccessful = true;
    vec3 vec3Property = vec3();
    std::string errorCode = "";

    RtUtilities::DebugLog("Testing: " + property + " for vec3; \n", RtUtilities::LogColor::Green);

    std::vector<float> convertedResult = RtUtilities::ConvertMultivecPropertyToVecSized(property, parsefloatGroups, convertSuccessful);

    if(!convertSuccessful){
        vec3Property = vec3();
        RtUtilities::DebugLog("ERROR: " + errorCode + " conversion to VEC3 of property '" + property + "' failed; check your spelling. \n", RtUtilities::Red);
    } else {
        float tempFloats[parsefloatGroups] = {};
        for(int i = 0; i < parsefloatGroups; i++){
            tempFloats[i] = convertedResult[i];
        }

        RtUtilities::DebugLog("SUCCESS: vec3: " + std::to_string(tempFloats[0]) + ":" + std::to_string(tempFloats[1]) + ":" + std::to_string(tempFloats[2]) + "\n", RtUtilities::White);
        convertSuccessful = true;
        vec3Property = vec3(tempFloats[0],tempFloats[1],tempFloats[2]);
    }

    return vec3Property;
}


color4 RtUtilities::ConvertPropertyToColor4(const std::string & property, bool &convertSuccessful){

    int parsefloatGroups = 4;
    convertSuccessful = true;
    color4 color4Property = color4();
    std::string errorCode = "";

    RtUtilities::DebugLog("Testing: " + property + " for color4; \n", RtUtilities::LogColor::Green);

    std::vector<float> convertedResult = RtUtilities::ConvertMultivecPropertyToVecSized(property, parsefloatGroups, convertSuccessful);

    if(!convertSuccessful){
        color4Property = color4();
        RtUtilities::DebugLog("ERROR: " + errorCode + " conversion to COLOR4 of property '" + property + "' failed; check your spelling. \n", RtUtilities::Red);
    } else {
        float tempFloats[parsefloatGroups] = {};
        for(int i = 0; i < parsefloatGroups; i++){
            tempFloats[i] = convertedResult[i];
        }

        RtUtilities::DebugLog("SUCCESS: color4: " 
            + std::to_string(tempFloats[0]) 
            + ":" + std::to_string(tempFloats[1]) 
            + ":" + std::to_string(tempFloats[2]) 
            + ":" + std::to_string(tempFloats[3]) 
            + "\n", RtUtilities::White);
        convertSuccessful = true;
        color4Property = color4(tempFloats[0],tempFloats[1],tempFloats[2], tempFloats[3]);
    }

    return color4Property;
}