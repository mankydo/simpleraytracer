#include <chrono>  // for high_resolution_clock
#include <iostream>
#include "image.h"
#include "Ray.h"
#include "SimpleRayRenderer.h"
#include "vec3.h"
#include "RtMath.h"
#include "IMaterial.h"
#include "RtUtilities.h"

//    color4 colorHit = color4(1.0, 0.0, 0.0, 1.0);
//    color4 colorMiss = color4(0.0, 0.0, 0.0, 0.0);
//    color4 colorEOL = color4(0.0, 0.0, 0.0, 1.0);

#define COLOR_HIT color4(1.0, 0.0, 0.0, 1.0)
#define COLOR_MISS color4(0.0, 0.0, 0.0, 0.0)
#define COLOR_EOL color4(0.0, 0.0, 0.0, 1.0)
#define COLOR_MATERIALMISSING color4(1.0, 0.0, 1.0, 1.0)

void SimpleRayRenderer::render(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters)
{
    //Initiating render parameters and objects
    IBackground *sceneBackground = scene.GetBackground();
    bool invert = in_renderParameters.inverty;
    int nx = image.getwidth();
    int ny = image.getheight();
    int numberOfSamples = in_renderParameters.samples;
    int bounces = in_renderParameters.bounces;
    std::vector<HittableObject *> hittableToRender = GetHittableObjectsToRender(scene, in_renderParameters.printProgress);

    // Record start time
    auto render_start = std::chrono::high_resolution_clock::now();
    std::time_t render_start_time = std::chrono::system_clock::to_time_t(render_start);

    if(in_renderParameters.printProgress)
        std::cout<<"\n Started rendering.\n";

    for(int j = 0; j < ny; j++){
        for(int i =0; i < nx; i++){
            float u = float(i) / float(nx);
            float v;
            //we should go from down up because the Y axis is looking up(origin in left bottom corner), 
            //  so a default behaviour is to substract from final pixels 
            //  since on a screen pixels are counted from up to down(origin in left up corner).
            if(!invert){
                v = float(ny - j)/float(ny);
            } else {
                v = float(j)/float(ny);
            }

            ray r = renderCamera.get_ray(u, v);
            color4 col = color4(0,0,0,0);

        //a naive antialiasing, soon(hopefully) to be moved out to a separate method(s)
            for(int sample = 0; sample < numberOfSamples; sample++){
                r = renderCamera.get_ray(
                    u + (RtMath::random_double()/ float(nx)), 
                    v + (RtMath::random_double()/ float(ny))
                );
                color4 foregroundObjects = RayColor(r, hittableToRender, sceneBackground, bounces);
                color4 backgroundColor = sceneBackground->GetColorAtDirection(r);
                //a simple hack to give out an alpha while retaining a background
                backgroundColor.a(0);
                if(in_renderParameters.TransparentBackground) {
                    col += foregroundObjects * foregroundObjects.a();
                } else {
                    col += backgroundColor * (1.0 - foregroundObjects.a())  + foregroundObjects * foregroundObjects.a();
                }
            }

            image.setpixel(i, j, col);
        }
        if(in_renderParameters.printProgress)
            RtUtilities::PrintProgress((float)j / ny);
    }

    if(in_renderParameters.printProgress)
        std::cout<<"\n Finished rendering.\n";

    // Record end time
    auto render_finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> render_duration = render_finish - render_start;
    std::time_t render_finish_time = std::chrono::system_clock::to_time_t(render_finish);

    image.setrenderstats(render_duration.count(), render_start_time, render_finish_time);
    image.setSamples(in_renderParameters.samples);
    if(in_renderParameters.normalizeResult)
        image.NormalizeBySamples();
}

color4 SimpleRayRenderer::RayColor(const ray &r, std::vector<HittableObject *> &objectsToRender, IBackground *background, int bounceMax, int currentBounce){

    color4 color = COLOR_MISS;

    std::vector<HittableObject *>::iterator iterate;
    iterate = objectsToRender.begin();
    bool result = false;
    double t_min = 0.0001;
    double closest_so_far = infinity;
//    double closest_so_far = MAXFLOAT;
    hit_record hit_info;
    hit_record temp_hit_info;
    ray rayToManipulate = r;

//    std::cout<<"checking hits_";
    while(iterate != objectsToRender.end()){

// here will be ray rotation per object:
        vec3 rayDirection = r.direction() * (*iterate)->RotationMatrix();
        vec3 rayOrigin = (r.origin() - (*iterate)->Position()) * (*iterate)->RotationMatrix();
        ray rayTranslated = ray(rayOrigin, unit_vector(rayDirection)); 

        if((*iterate)->Hit(rayTranslated, t_min , closest_so_far, temp_hit_info)){
            result = true;
            closest_so_far = temp_hit_info.t;
            hit_info = temp_hit_info;

            rayToManipulate = r;

            hit_info.p = rayToManipulate.point_at_parameter(hit_info.t);

    // un-rotating normals of the object to get them in place by multiplying rotated normals by an inversed rotation matrix
    // It's actually a transpose of a rotation matrix because in case of rotation: R^(-1) = R^T
    // i.e. an inverse matrix is equal to a transposed matrix since it's determinant is equal to 1

            hit_info.normal = hit_info.normal * (*iterate)->RotationMatrixInverse();

            hit_info.setFaceNormal(rayToManipulate, hit_info.normal);
        }
        iterate++;
    }

    if(result){
        if(currentBounce == bounceMax){
            color = COLOR_EOL;
        } else {
//            vec3 target = hit_info.p + hit_info.normal + RandomInUnitSphere();
//            vec3 target = hit_info.p + hit_info.normal + RandomUnitVector();
            ray scatteredRay;
            color4 attenuation;
            if(hit_info.materialPointer){
                if(hit_info.materialPointer->Scatter(rayToManipulate, hit_info, attenuation, scatteredRay) ) {
                    color = attenuation * RayColor(
                            scatteredRay,
                            objectsToRender, 
                            background,
                            bounceMax,
                            currentBounce + 1
                        );
                } else {
                    color = COLOR_EOL;
                }
            } else {
                color = COLOR_MATERIALMISSING;
            }
        }
    } else {
        if(currentBounce != 0) {
            color = background->GetColorAtDirection(rayToManipulate);
        } else {
            color = COLOR_MISS;
        }
    }

    return color;
}

std::vector<HittableObject *> SimpleRayRenderer::GetHittableObjectsToRender(const Scene &in_scene, bool printDebug){
    std::vector<Object *> objectsToRender = in_scene.GetObjectsTypeOf(hittableObjects);
    std::vector<Object *>::iterator i;
    std::vector<HittableObject *> hittableToRender;
    for(i = objectsToRender.begin(); i != objectsToRender.end(); i++){
        if( (*i)->GetObjectType() == hittableObjects){
            hittableToRender.push_back( (HittableObject *)(*i));

            RtUtilities::DebugLog("Object: " + (*i)->GetName() + " added to a list to render\n", RtUtilities::LogColor::Grey);
        }
    }
    RtUtilities::DebugLog(std::to_string(hittableToRender.size()) + " elements added for rendering\n");

    return hittableToRender;
}

void SimpleRayRenderer::renderBackground(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters)
{
    //Initiating render parameters and objects
    IBackground *sceneBackground = scene.GetBackground();
    bool invert = in_renderParameters.inverty;
    int nx = image.getwidth();
    int ny = image.getheight();

    // Record start time
    auto render_start = std::chrono::high_resolution_clock::now();
    std::time_t render_start_time = std::chrono::system_clock::to_time_t(render_start);

    if(in_renderParameters.printProgress)
        std::cout<<"\n Started rendering.\n";

    for(int j = 0; j < ny; j++){
        for(int i =0; i < nx; i++){
            float u = float(i) / float(nx);
            float v;
            //we should go from down up because the Y axis is looking up(origin in left bottom corner), 
            //  so a default behaviour is to substract from final pixels 
            //  since on a screen pixels are counted from up to down(origin in left up corner).
            if(!invert){
                v = float(ny - j)/float(ny);
            } else {
                v = float(j)/float(ny);
            }

            ray r = renderCamera.get_ray(u, v);
            color4 backgroundColor = sceneBackground->GetColorAtDirection(r);

            image.setpixel(i, j, backgroundColor);
        }
        if(in_renderParameters.printProgress)
            RtUtilities::PrintProgress((float)j / ny);
    }

    if(in_renderParameters.printProgress)
        std::cout<<"\n Finished rendering.\n";

    // Record end time
    auto render_finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> render_duration = render_finish - render_start;
    std::time_t render_finish_time = std::chrono::system_clock::to_time_t(render_finish);

    image.setrenderstats(render_duration.count(), render_start_time, render_finish_time);
}