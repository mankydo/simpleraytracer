#include <chrono>  // for high_resolution_clock
#include <iostream>
#include "image.h"
#include "TestGridRenderer.h"

void TestGridRenderer::render(image &image, const Scene &scene, const Camera &renderCamera, RenderParameters in_renderParameters)
{
    bool invert = in_renderParameters.inverty;
    bool add_grid = in_renderParameters.add_grid;
    int nx = image.getwidth();
    int ny = image.getheight();


    // Record start time
    auto render_start = std::chrono::high_resolution_clock::now();
    std::time_t render_start_time = std::chrono::system_clock::to_time_t(render_start);

    //generate the gradient
    for(int j = 0; j < ny; j++){
        for(int i =0; i < nx; i++){
            float r = float(i)/float(nx);
            float g;
            if(invert){
                g = float(j)/float(ny);
            } else {
                g = float(ny - j)/float(ny);
            }
            float b = .2;

            if(add_grid && (i%20 == 0 || j%20 == 0 )){
                b+=.9;
            } 

            color4 col(r, g, b, 1);

            image.setpixel(i, j, col);
        }
    }


    // Record end time
    auto render_finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> render_duration = render_finish - render_start;
    std::time_t render_finish_time = std::chrono::system_clock::to_time_t(render_finish);

    image.setrenderstats(render_duration.count(), render_start_time, render_finish_time);

}
