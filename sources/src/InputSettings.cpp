#include "InputSettings.h"

void InputSettings::ParseAndUpdateSettings(int argc, char** argv){
        std::string arg_inverty = "inverty";
        std::string arg_grid = "addgrid";
        std::string arg_saveAsBmp = "bmp";
        std::string arg_useRays = "userays";
        std::string arg_useTestGrid = "usetestgrid";
        std::string arg_debugInfo = "debuginfo";
        std::string arg_output2d = "output2d";              


        for(int i = 1; i < argc; i++){

            if(argv[i] == arg_inverty){
                std::cout << "inverting Y axis\n";
                this->in_invert = true;
            }

            if(argv[i] == arg_grid){
                std::cout << "Adding blue grid\n";
                this->in_add_grid = true;
            }

            if(argv[i] == arg_saveAsBmp){
                std::cout << "Trying to save as .bmp file\n";
                this->in_saveAsBmp = true;
            }
            if(argv[i] == arg_useRays){
                std::cout << "Using rays for rendering\n";
                this->in_userays = true;
            }
            if(argv[i] == arg_useTestGrid){
                std::cout << "Using test grid for rendering\n";
                this->in_userays = false;
            }
            if(argv[i] == arg_debugInfo){
                std::cout << "Debug output is on\n";
                this->debugLogOutput = true;
            }
            if(argv[i] == arg_output2d){
                std::cout << "2d output module is on(instead of 3d rendering)\n";
                this->output2d = true;
            }

            std::string inputArg = argv[i];

            if( inputArg.substr(0, 3) == "--d" ){
                this->ParseDimensions(inputArg);
                this->isChangedDimensions = true;
            }

            if( inputArg.substr(0, 9) == "--samples" ){
                this->ParseSamples(inputArg);
                this->isChangedSamples = true;
           }

            if( inputArg.substr(0, 9) == "--bounces" ){
                this->ParseBounces(inputArg);
                this->isChangedBounces = true;
           }
            if( inputArg.substr(0, 4) == "--s:" ){
                this->settingsFilename = inputArg.substr(4);
           }
        }

}

void InputSettings::ParseDimensions(std::string inputArg){
    int positionXStart = inputArg.find("--d:") + 4;
    int positionXEnd = inputArg.find("x");
    std::string xDimensionString = inputArg.substr(positionXStart, positionXEnd - positionXStart);
    std::string yDimensionString = inputArg.substr(positionXEnd + 1);

    std::cout << "Requested to change dimentions to: " << xDimensionString << " x " << yDimensionString << " \n";

    bool dimensionError = false;
    int xDimensionTemp, yDimensionTemp;
    try
    {
        xDimensionTemp = std::stoi(xDimensionString);
    }
    catch(const std::invalid_argument& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "conversion of X failed \n";
        dimensionError = true;
    }
    try
    {
        yDimensionTemp = std::stoi(yDimensionString);
    }
    catch(const std::invalid_argument& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "conversion of Y failed \n";
        dimensionError = true;
    }
    if(dimensionError){
        std::cout << "ERROR: Cant convert input dimentions to integer numbers; Check your spelling\n";
    } else {
        this->dimensionX = xDimensionTemp;
        // just to make compiler happy as for now the aspect ratio is tied to X dimension
        this->dimensionY = yDimensionTemp;
//        this->dimensionY =  this->dimensionX / 2;
    }
}

void InputSettings::ParseSamples(std::string inputArg){
    int positionStart = inputArg.find("--samples:") + 10;
    std::string samplesString = inputArg.substr(positionStart);

    std::cout << "Requested to change samples to: " << samplesString << " \n";

    try
    {
        this->numberOfSamples = std::stoi(samplesString);
    }
    catch(const std::invalid_argument& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: conversion of samples failed; check your spelling. \n";
    }
    std::cout << "Set samples at: " << this->numberOfSamples << " \n";
}

void InputSettings::ParseBounces(std::string inputArg){
                int positionStart = inputArg.find("--bounces:") + 10;
                std::string bouncesString = inputArg.substr(positionStart);

                std::cout << "Requested to change bounces to: " << bouncesString << " \n";

                try
                {
                    this->numberOfBounces = std::stoi(bouncesString);
                }
                catch(const std::invalid_argument& e)
                {
                    std::cerr << e.what() << '\n';
                    std::cout << "ERROR: conversion of bounces failed; check your spelling. \n";
                }
                std::cout << "Set bounces at: " << this->numberOfBounces << " \n";
 
}
