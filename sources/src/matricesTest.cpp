#include "RtMath.h"
#include "RtUtilities.h"

void MatricesTest()
{
    Matrix3x3 matrixToInverse = Matrix3x3();
    for(int i = 0; i < 9; i++){
        matrixToInverse.e[i - (int)((i) / 3) * 3][(int)(i / 3)] = i;
    }

/*     for(int i = 0; i < 9; i++){
        matrixToInverse.e[i - (int)((i) / 3) * 3][(int)(i / 3)] *= (i % 2 == 0 ? 1 : -1);
//        minorMatrix.e[i - (int)((i) / 3) * 3][(int)(i / 3)] *= 4 * (i % 2) - 1;
    } */

     for(int i = 0; i < 9; i++){
//        matrixToInverse.e[i - (int)((i) / 3) * 3][(int)(i / 3)] *= -1 * std::div(i,2).rem;
        matrixToInverse.e[i - (int)((i) / 3) * 3][(int)(i / 3)] *= 1  - (i % 2) * 2;
    } 


    RtUtilities::Log("\n\nInitinal matrix is: ");
    for(int i = 0; i < 3; i++){
        RtUtilities::Log("\n");
        for( int j = 0; j < 3; j++){
            RtUtilities::Log(std::to_string(matrixToInverse.e[i][j]) + ":");
        }
    }
    RtUtilities::Log("\n");

Matrix3x3 minorMatrix = Matrix3x3();

//find a matrix of minors
    for(int i = 0; i < 3; i++){
        for( int j = 0; j < 3; j++){
            minorMatrix.e[i][j] = RtMath::Matrices::DeterminantForElement(matrixToInverse, i, j);
        }
    }


    RtUtilities::Log("\n\nminor matrix is: ");
    for(int i = 0; i < 3; i++){
        RtUtilities::Log("\n");
        for( int j = 0; j < 3; j++){
            RtUtilities::Log(std::to_string(minorMatrix.e[i][j]) + ":");
        }
    }
    RtUtilities::Log("\n");


}