#include <iostream>
#include <chrono>
#include "GlobalVariables.h"
#include "Utilities.h"
#include "save_ppm_image.h"
#include "vec3.h"
#include "image.h"

#include "scene.h"
#include "Materials.h"
#include "ObjectManager.h"
#include "sphere.h"
#include "plane.h"
#include "box.h"

#include "ISaveImage.h"
#include "IRenderer.h"
#include "TestGridRenderer.h"
#include "SimpleRayRenderer.h"
#include "SimpleGradientBackground.h"

#include "NgonUniformDistributionRenderer.h"

#include "InputSettings.h"
#include "RtMultithreading.h"
#include "Parsers.h"
#include "DefaultSceneCreation.h"


bool g_DebugLog = false;

int main(int argc, char** argv){

    InputSettings _inputSettings = InputSettings();

    RtUtilities::Log("This program outputs a raytraced image in a .ppm format file.\n");
    RtUtilities::Log("Render settings in a setting file are overrided by corresponding command-line options if there are ones.\n");
    RtUtilities::Log("This program has several command-line arguments to use:\n");
        RtUtilities::Log(" - 'inverty': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to flip a gradient in Y direction\n");
        RtUtilities::Log(" - 'addgrid'", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log(": to add grid to gradient\n");
        RtUtilities::Log(" - 'bmp': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to save as .bmp file instead of .ppm\n");
        RtUtilities::Log(" - 'userays': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to use ray tracing renderer(default)\n");
        RtUtilities::Log(" - 'usetestgrid': ", 2, RtUtilities::LogColor::Orange);
        RtUtilities::Log("to use test grid renderer\n");
        RtUtilities::Log(" - 'debuginfo': ", 2, RtUtilities::LogColor::Orange);
        RtUtilities::Log("for, erm, debug info.\n");
        RtUtilities::Log(" - 'output2d': ", 2, RtUtilities::LogColor::Orange);
        RtUtilities::Log("for, erm, output 2d mode instead of proper raytrace rendering.\n");
        RtUtilities::Log(" - '--d:XXXxYYY': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to use custom image dimensions; default: ");
            RtUtilities::Log( std::to_string(_inputSettings.dimensionX) + "x" + std::to_string(_inputSettings.dimensionY) + "\n", RtUtilities::LogColor::Grey);
        RtUtilities::Log(" - '--samples:SS': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to use custom number of samples; default: ");
            RtUtilities::Log( std::to_string(_inputSettings.numberOfSamples) + "\n", RtUtilities::LogColor::Grey);
        RtUtilities::Log(" - '--bounces:SS': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to use custom number of bounces; default: ");
            RtUtilities::Log(std::to_string(_inputSettings.numberOfBounces) + "\n", RtUtilities::LogColor::Grey);
        RtUtilities::Log(" - '--s:FILENAME': ", 2, RtUtilities::LogColor::Orange);
            RtUtilities::Log("to use settings stored in a file; default: ");
            RtUtilities::Log(_inputSettings.settingsFilename + "\n", 2);
    //In future add a setting to enable per-pass rendering(when or if it will be done)

    std::cout << "\n\n";
    std::cout << "Got " << argc - 1 << " arguments" << std::endl;


    if(argc > 1){  
        _inputSettings.ParseAndUpdateSettings(argc, argv);
        g_DebugLog = _inputSettings.debugLogOutput;
    }
    std::cout << "\n";

    image* img = new image(_inputSettings.dimensionX, _inputSettings.dimensionY);

    ISaveImage* saveImage;
    if(!_inputSettings.in_saveAsBmp){
        saveImage = new SaveAsPPMImage();
    } else {
        std::cout << "\n    ERROR: Cannot save to .bmp at the time, saving to .ppm instead\n\n";
        saveImage = new SaveAsPPMImage();
    } 

    //Render parameters
    RenderParameters renderParameters = {};
    renderParameters.width = _inputSettings.dimensionX;
    renderParameters.height = _inputSettings.dimensionY;
    renderParameters.add_grid = _inputSettings.in_add_grid;
    renderParameters.inverty = _inputSettings.in_invert;
    renderParameters.samples = _inputSettings.numberOfSamples;
    renderParameters.bounces = _inputSettings.numberOfBounces;

    IRenderer* renderer;
    if(_inputSettings.in_userays) {
        renderer = new SimpleRayRenderer();
    } else
    {
        renderer = new TestGridRenderer();
    }
    
    if(_inputSettings.output2d){
        NgonTestRenderer ngonRenderer = NgonTestRenderer();
        ngonRenderer.render(*img, renderParameters.samples);
        std::cout << "  Render lasted:        " << img->getrendertime() << "\n";
        image out = img->Crop(400, 300);
        saveImage->saveAsImage(*img, "render_ngon");
        saveImage->saveAsImage(out, "render_ngon_crop");
        return 0;
    }

    //Objects setup 
    //create Object manager;
    //create a scene to render;
    //create objects (via object manager by inheritance) and add them to a scene
    
    ObjectManager::Instance()->Init();

    std::string sceneToRenderName = "";

    //parse scene and object description from a file
    std::vector<std::string> errors = std::vector<std::string>();
    RenderParameters parsedRenderParameters = {};
    parsedRenderParameters.add_grid = _inputSettings.in_add_grid;
    parsedRenderParameters.inverty = _inputSettings.in_invert;
   
    ParseObjectSettings::ParseAndApplyFileSettings(_inputSettings.settingsFilename, errors, parsedRenderParameters);
    for(std::string error : errors){
        RtUtilities::Log( "[ERROR]: " + error + "\n", RtUtilities::LogColor::Red);
    }
    //overriding file settings if there are corresponding command line arguments
    if(_inputSettings.isChangedDimensions && _inputSettings.settingsFilename != ""){
        parsedRenderParameters.width = _inputSettings.dimensionX;
        parsedRenderParameters.height = _inputSettings.dimensionY;
        parsedRenderParameters.cameraToRender->SetAspectRatio(parsedRenderParameters.width, parsedRenderParameters.height);
    }
    if(_inputSettings.isChangedSamples){
        parsedRenderParameters.samples = _inputSettings.numberOfSamples;
    }
    if(_inputSettings.isChangedBounces){
        parsedRenderParameters.bounces = _inputSettings.numberOfBounces;
    }


    Scene * scene;
    Camera * cameraToRender;

    if(_inputSettings.settingsFilename == ""){
        scene = DefaultSceneCreation(renderParameters);
        cameraToRender = new Camera("camera_1", renderParameters.width, renderParameters.height, 130);
        //cameraToRender->SetFOV(50);
        //cameraToRender->SetPosition(vec3(-.2, 0.1, 5));
    } 
    else {
        renderParameters = parsedRenderParameters;
        scene = renderParameters.sceneToRender;
        cameraToRender = renderParameters.cameraToRender;
    }

//    MatricesTest();

    if(scene == nullptr){
        RtUtilities::Log("[ERROR] There is no scene named '" + sceneToRenderName + "' to render, exiting.\n", RtUtilities::LogColor::Red);
        return 0;
    }

    std::cout << "\nTrying to estimate render time:\n";
    int scaleTestImg = 4;
    image* oneSampleImg = new image(renderParameters.width / scaleTestImg, renderParameters.height / scaleTestImg);
    RenderParameters estimateRenderParameters = renderParameters;
    estimateRenderParameters.samples = 1;
    estimateRenderParameters.printProgress = false;
    renderer->render(*oneSampleImg, *scene, *cameraToRender, estimateRenderParameters);
    float estimatedTime = oneSampleImg->getrendertime() * renderParameters.samples * (scaleTestImg * scaleTestImg);
    std::cout << "Estimated(single-threaded) time for render is: " << estimatedTime << "s \n\n\n";


/*     for(auto i: scene->GetSceneObjects()){
        RtUtilities::DebugLog("Got an object: " + i->GetName() + " with a type of " + std::to_string(i->GetObjectType()) + "\n", RtUtilities::LogColor::Orange);
    }
 */


    //Actual render happens here
//    renderer->render(*img, scene, cameraToRender, renderParameters);

    *img = RaytraceMultithread::RenderMultithreaded(renderer, *scene, *cameraToRender, renderParameters);

    double rendertime = img->getrendertime();
    double renderstart = img->getrenderstart();
    double renderend = img->getrenderend();

/*      image back = image(_inputSettings.dimensionX, _inputSettings.dimensionY);
    back.fillWithColor(color4(.5,0, 0, 1));

    IRenderer * gridRenderer = new TestGridRenderer();
    gridRenderer->render(back, scene, cameraToRender, renderParameters);
    delete gridRenderer;

    back.AddWithAlpha(*img);

    saveImage->saveAsImage(back, "renderBack");
 */
    saveImage->saveAsImage(*img, "render");

    delete saveImage;
    delete oneSampleImg;
    delete img;
    delete renderer;
    ObjectManager::Instance()->DeleteAllObjects();

    std::cout << "\n\n";
    std::cout << "  Render lasted:        " << rendertime << "\n";
    std::cout << "  Render start time is:    " << renderstart << "\n";
    std::cout << "  Render end time is:      " << renderend << "\n";

    std::cout<<"Finished.\n";
}