#include "Materials.h"

MaterialLambert::MaterialLambert(std::string name, color4 albedo): Material(name)
{
    this->Name = name;
    this->_Albedo = albedo;
}

MaterialLambert::~MaterialLambert() {}

bool MaterialLambert::Scatter(
        const ray &inRay, 
        const hit_record & hitInfo, 
        color4 &attenuation, 
        ray &scatteredRay
    ) const
    {
        vec3 scatterDirection = hitInfo.normal + RtMath::RandomUnitVector();
        scatteredRay = ray(hitInfo.p, scatterDirection);
        attenuation = this->GetAlbedo();
        return true;
    }
    
color4 MaterialLambert::GetAlbedo() const{
    return _Albedo;
}

MaterialMetallic::MaterialMetallic(std::string name, color4 albedo, float metallic): Material(name)
{
    this->Name = name;
    this->_Albedo = albedo;
    this->_Metallic = clamp01(metallic);
}

MaterialMetallic::~MaterialMetallic() {}

bool MaterialMetallic::Scatter(
        const ray &inRay, 
        const hit_record & hitInfo, 
        color4 &attenuation, 
        ray &scatteredRay
    ) const
    {
        vec3 reflectDirection = Reflect(unit_vector(inRay.direction()), hitInfo.normal);
        scatteredRay = ray(hitInfo.p, reflectDirection + (1 - _Metallic) * RtMath::RandomInUnitSphere());
//        scatteredRay = ray(hitInfo.p, reflectDirection + (1 - _Metallic) * RandomUnitVector());
        attenuation = this->GetAlbedo();
        return (dot(scatteredRay.direction(), hitInfo.normal) > 0);
    }
    
color4 MaterialMetallic::GetAlbedo() const{
    return _Albedo;
}