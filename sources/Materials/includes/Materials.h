#ifndef MATERIALLAMBERT_H
#define MATERIALLAMBERT_H

#include "Material.h"
#include "Utilities.h"
#include "RtMath.h"

class MaterialLambert : public Material
{
private:
    color4 _Albedo;

public:
    MaterialLambert(std::string name, color4 albedo);
    bool Scatter(
        const ray &inRay, 
        const hit_record & hitInfo, 
        color4 &attenuation, 
        ray &scatteredRay
    ) const override;
    
    color4 GetAlbedo() const;

    ~MaterialLambert();
};

class MaterialMetallic : public Material
{
private:
    color4 _Albedo;
    float _Metallic = 1;

public:
    MaterialMetallic(std::string name, color4 albedo, float metallic = 1);
    bool Scatter(
        const ray &inRay, 
        const hit_record & hitInfo, 
        color4 &attenuation, 
        ray &scatteredRay
    ) const override;
    
    color4 GetAlbedo() const;

    ~MaterialMetallic();
};


#endif