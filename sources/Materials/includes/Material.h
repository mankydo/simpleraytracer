#ifndef MATERIAL_H
#define MATERIAL_H

#include "IMaterial.h"
#include "Object.h"


class Material: public IMaterial, public Object
{
    public:
        Material(std::string name);

 // Later lines is a copy/paste job from IMaterial so it would be more visualy clear what is going on       
 //
 // Maybe later on IMaterial would be deleted at all since it's not needed at 
 // this stage at all but for the sake of name conventions I'll leave it as it is for now

        virtual bool Scatter(
            const ray &inRay, 
            const hit_record & hitInfo, 
            color4 &attenuation, 
            ray &scatteredRay
        ) const = 0;
    
        virtual ~Material(){};

};

#endif