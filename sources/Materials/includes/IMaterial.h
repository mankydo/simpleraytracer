#ifndef IMATERIAL_H
#define IMATERIAL_H

#include "color4.h"
#include "Ray.h"
#include "IHittable.h"


class IMaterial
{
public:
    std::string Name;

    virtual bool Scatter(
        const ray &inRay, 
        const hit_record & hitInfo, 
        color4 &attenuation, 
        ray &scatteredRay
    ) const = 0;
    
    virtual ~IMaterial(){};
};

#endif