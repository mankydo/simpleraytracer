//an example settings file
// color format: 1,1,1,1
// position format: 1,1,1
// booleans: 1 for true, 0 for false

<settings>
	[width]:1000
	[height]:400
	[samples]:10
	[bounces]:20
	[scene]:test_scene2
	[camera]: camera_to_render
</settings>

<camera>
	[name]:camera_to_render
	[position]: 0, .4, 4
	[rotation]: .1, 0, 0
	[fov]: 30
	[dof_enabled]: 0
	[focalpoint]: -.208, .22, 0
	[aperture]: .2
	[blades_enabled]: 1
        [blades_number]: 5
        [blades_rotation]: .2
</camera>

<scene>
	[name]: test_scene2
	[background]: test_background
	[addobject]: test_box
	[addobject]: test_box_mirror
	[addobject]: test_box_mirror2
	[addobject]: sphere_z_3
//	[addobject]: sphere_z_4
//	[addobject]: sphere_z_big
	[addobject]: sphere_z_cyan_below
	[addobject]: focal_plane_test
//	[addobject]: focal_plane_test2
</scene>

<background>
	[name]: test_background
	[colorup]: .5, .7, 1.0,1
	[colorbottom]: 1.0, 1.0, 1.0,1
</background>

<material>
	[name]:cyan lambert
	[type]:lambert
	[color]:.2, .7, 1.0, 1
	[roughness]: 1
</material>
<material>
	[name]:red lamberttt
	[type]:lambert
	[color]:.8, .2, .2, 1
	[roughness]: 1
</material>
<material>
	[name]:metallic red
	[type]: metallic
	[color]: .8, .1, .2, 1
	[roughness]: .4
</material>

<material>
	[name]:metallic blue
	[type]: metallic
	[color]: .5, .5, .8, 1
	[roughness]: .98
</material>

<object>
	[name]:focal_plane_test
	[type]: plane
	[position]: 0,-.3,0
	[material]: red lamberttt
	[planenormal]: 0, -1, 0
</object>
<object>
	[name]:focal_plane_test2
	[type]: plane
	[position]: -.48, -.2, -1.6
	[material]: metallic blue
	[planenormal]: 0, 0, -1
</object>
<object>
	[name]:test_box
	[type]: box
	[position]: 0,0,0
//	[position]: -0.2,-.1,-.2
	[material]: cyan lambert
//	[dimensions]: .15,.6,.5
	[dimensions]: .3,.5,.5
	[rotation]: 45, 500, 0
//	[rotation]: 0, 1.3, 0
//	[rotation]: 0, 0, 0
</object>
<object>
	[name]:sphere_z_3
	[type]: sphere
	[position]: -.208, .42, 0
	[material]: metallic red
	[radius]: 0.15
</object>
<object>
	[name]:sphere_z_4
	[type]: sphere
	[position]: -.508, -.12, -.04
	[material]: metallic blue
	[radius]: 0.15
</object>
<object>
	[name]:test_box_mirror
	[type]: box
	[position]: -0.55,-.1,-.5
	[material]: metallic blue
	[dimensions]: .1,.5,2.5
	[rotation]: 0, -23, 0
</object>
<object>
	[name]:test_box_mirror2
	[type]: box
	[position]: 0.55,-.1,-.5
	[material]: metallic blue
	[dimensions]: .1,.5,2.5
	[rotation]: 0, 23, 0
</object>
<object>
	[name]:sphere_z_big
	[type]: sphere
//	[position]: .478, .04, 0
	[position]: 2.478, .04, -.3
	[material]: metallic blue
//	[radius]: 0.15
	[radius]: 2.15
</object>
<object>
	[name]:sphere_z_cyan_below
	[type]: sphere
	[position]: -.308, -.022, 0.1
	[material]: cyan lambert
	[radius]: 0.15
</object>
